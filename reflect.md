https://pkg.go.dev/reflect

helps to get the underlying type of interfaces

```
fmt.Println("Type:", reflect.TypeOf(myvar))
fmt.Println("Value:", reflect.ValueOf(myvar))
```

#### reflect.TypeOf

```
func TypeOf(i any) Type
    TypeOf returns the reflection Type that represents the dynamic type of i.
    If i is a nil interface value, TypeOf returns nil.
```

example:
```
my_int := 3
type_of_int := reflect.TypeOf(my_int)

fmt.Printf("%s (%T)\n", type_of_int, type_of_int)
```
outputs:
```
int (*reflect.rtype)
```
