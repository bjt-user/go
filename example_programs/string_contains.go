package main

import (
	"fmt"
	"strings"
)

func main() {
	my_string := "hello"
	contains_h := strings.Contains(my_string, "h")
	fmt.Println(contains_h)
}