package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

func inspect_main(main_fn ast.Node) {
	fn, ok := main_fn.(*ast.FuncDecl)
	if !ok {
		return
	}

	fmt.Println("Inspecting main function")
	fmt.Printf("Main starts at character %v\n", fn.Pos())
	fmt.Printf("and ends at character %v\n", fn.End())
}

func main() {
	src := `
package main

var globvar int = 5

func lets_fail() {
	fmt.Fatalf("Foo...")
}

func print_help() {
	help_string := "HELP ME!"
	fmt.Println(help_string)
	lets_fail()
}

func main() {
	fmt.Println("Hello, World")
	print_help()
}

func main() {
	fmt.Println("a second main")
}`

	fset := token.NewFileSet()

	ast_file, _ := parser.ParseFile(fset, "", src, parser.AllErrors)

	for _, v := range ast_file.Decls {
		fn, ok := v.(*ast.FuncDecl)
		if !ok {
			continue
		}
		fmt.Printf("%v (%T)\n", fn, fn)
		if fn.Name.Name == "main" {
			//			fn_start := fn.Body.List[0].Pos()
			//			fn_end := fn.Body.List[len(fn.Body.List)-1].End()

			fmt.Printf("Function body: From char %v to %v\n",
				fn.Body.List[0].Pos(), fn.Body.List[len(fn.Body.List)-1].End())

			fmt.Println(src[fn.Body.Pos():fn.Body.End()])

			inspect_main(fn)
			break
		}
	}
}
