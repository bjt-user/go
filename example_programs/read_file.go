package main

import "os"
import "log"

func main() {
	content, err := os.ReadFile("file.txt")
	if err != nil {
		log.Fatal(err)
	}
	
	println(string(content))
}
