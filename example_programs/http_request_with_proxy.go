package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
)

// this program also works when you do not use a proxy

func main() {
	dest_url, err := url.Parse("http://example.com")

	if err != nil {
		panic(err)
	}

	var my_request http.Request

	my_request.Method = "GET"
	my_request.URL = dest_url

	if &my_request == nil {
		panic("Your request is nil???")
	}

	proxy_url, err := http.ProxyFromEnvironment(&my_request)

	if err != nil {
		panic(err)
	}

	if proxy_url != nil {
		fmt.Printf("proxy_url: %v\n", proxy_url)
	}
	if dest_url != nil {
		fmt.Printf("dest_url: %v\n", dest_url)
	}

	// Create a Transport with the proxy
	my_transport := &http.Transport{
		Proxy: http.ProxyURL(proxy_url),
	}

	my_client := http.Client{
		Transport: my_transport,
	}

	fmt.Printf("my_client: %#v\n", my_client)

	resp, err := my_client.Do(&my_request)

	if err != nil {
		panic(err)
	}

	fmt.Printf("resp: %#v\n", resp)
	fmt.Printf("resp.Body: %#v\n", resp.Body)

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	fmt.Printf("\nbody:\n%s\n", string(body))
}
