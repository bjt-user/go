// You can pass a file name without prefixing a flag.
// Opposed to package flag flags that are passed after non-flags
// are still detected.
// Supports long and short options by using a simple switch case.
// Unknown flags will be caught.

package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	var verbose bool
	var file_name string

	for i, v := range os.Args[1:] {
		fmt.Printf("%v: %v\n", i, v)

		switch v {
		case "-V", "--version":
			fmt.Printf("Version 1.\n")
			os.Exit(0)
		case "-v", "--verbose":
			verbose = true
		default:
			if strings.HasPrefix(v, "-") {
				fmt.Printf("Unknown flag %v\n", v)
				os.Exit(1)
			}
			file_name = v
		}
	}

	fmt.Println("Printing Flags:")
	fmt.Printf("verbose: %v\n", verbose)
	fmt.Printf("file_name: %v\n", file_name)
}
