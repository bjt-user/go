package main

// this doubly linked list has no tail
// so you cannot traverse it from the last element
// but you can access the previous node in loops
// which can be helpful in some situations

// Note: You can't seem to define methods with
// double pointer receivers in Go, so I had to pass
// the double pointers as arguments.

import "fmt"

type person struct {
	name string
	age  int
	next *person
	prev *person
}

func pop(head **person, name string, age int) {
	new_node := &person{
		name: name,
		age:  age,
		next: *head,
		prev: nil,
	}

	if *head != nil {
		(**head).prev = new_node
	}

	*head = new_node
}

func (head *person) print_list() {
	cur_node := head

	for cur_node != nil {
		fmt.Printf("%+v\n", cur_node)
		cur_node = cur_node.next
	}
}

func main() {
	var head *person = nil

	pop(&head, "Steve", 44)
	pop(&head, "Frank", 33)
	pop(&head, "Carl", 33)

	head.print_list()
}
