package main

import (
    "fmt"
    "net/http"
    "io/ioutil"
)

func main() {
    // Send GET request
    response, err := http.Get("http://example.com/")
    if err != nil {
        fmt.Println("Error:", err)
        return
    }
    defer response.Body.Close()

    // Read response body
    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        fmt.Println("Error reading body:", err)
        return
    }

    // Convert response body to string
    html := string(body)

    // Print HTML content
    fmt.Println(html)
}