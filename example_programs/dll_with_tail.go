package main

// doubly linked list with tail

import "fmt"

type person struct {
	name string
	age  int
	next *person
	prev *person
}

func pop(head **person, tail **person, name string, age int) {
	new_node := &person{
		name: name,
		age:  age,
		next: *head,
		prev: nil,
	}

	if *head != nil {
		(*head).prev = new_node
	} else {
		// when the first element is added to the list
		// both head and tail point to it
		*tail = new_node
	}

	for cur_node := *head; cur_node != nil; cur_node = cur_node.next {
		if cur_node.next == nil {
			*tail = cur_node
		}
	}

	*head = new_node
}

func append_to_list(head **person, tail **person, name string, age int) {
	if *tail == nil {
		pop(head, tail, name, age)
	}

	new_node := &person{
		name: name,
		age:  age,
		next: nil,
		prev: *tail,
	}

	(*tail).next = new_node

	*tail = new_node
}

// TODO: print list from tail function

func (head *person) print_list() {
	cur_node := head

	for cur_node != nil {
		fmt.Printf("%+v\n", cur_node)
		cur_node = cur_node.next
	}
}

func main() {
	var head *person = nil
	var tail *person = nil

	pop(&head, &tail, "Steve", 44)
	pop(&head, &tail, "Frank", 33)
	pop(&head, &tail, "Carl", 33)

	head.print_list()

	println()

	fmt.Printf("head: %+v\n", head)
	fmt.Printf("tail: %+v\n", tail)
}
