package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
)

func strmap_to_ast(srcs map[string]string) ([]ast.File, *token.FileSet) {
	ast_files := make([]ast.File, 0)

	fset := token.NewFileSet()

	for file_name, file_content := range srcs {
		af, err := parser.ParseFile(fset, file_name, file_content, parser.SkipObjectResolution)
		if err != nil {
			log.Fatalf("parser.ParseFile failed.\n%s", err)
		}
		ast_files = append(ast_files, *af)
	}

	return ast_files, fset
}

func main() {
	srcs := make(map[string]string, 2)

	srcs["main.go"] = `package main

func main() {
	my_string := "foo"
	hello(my_string)
}`

	srcs["hello.go"] = `package main

import (
	"fmt"
)

func hello(greetstr string) {
	fmt.Println("hello", greetstr)
}`

	_, fset := strmap_to_ast(srcs)

	my_position := fset.Position(160)

	fmt.Printf("Column: %v\n", my_position.Column)
	fmt.Printf("Line: %v\n", my_position.Line)
	fmt.Printf("Offset: %v\n", my_position.Offset)
	fmt.Printf("IsValid(): %v\n", my_position.IsValid())
	fmt.Printf("String(): %v\n", my_position.String())
}
