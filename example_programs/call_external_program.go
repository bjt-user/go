package main

import (
	"fmt"
	"os/exec"
)

func ext_cmd(executable string, args ...string) string {
	cmd := exec.Command(executable, args...)

	fmt.Printf("Command to be executed: %v\n", cmd.String())

	out, err := cmd.Output()

	if err != nil {
		fmt.Println("could not run command: ", err)
	}

	return string(out)
}

func main() {
	output := ext_cmd("ls", "-la")

	fmt.Printf("%v\n", output)
}
