package main

/* this program reads a file, saves the content in a string
and then splits the string by new lines into a string slice
to access lines in a file in a simple way */

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	file_content_raw, err := os.ReadFile("test.md")

	if err != nil {
		log.Fatal(err)
	}

	var file_content string = string(file_content_raw)

	println(file_content)

	file_lines := strings.Split(file_content, "\n")

	for index, value := range file_lines {
		fmt.Println("line", index, "-", value)
	}

	println(file_lines[2])
}
