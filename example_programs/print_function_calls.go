package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

func main() {
	// create a fileset to work with
	fset := token.NewFileSet()
	// parse the file and create an ast
	file, err := parser.ParseFile(fset, "src.go", nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}
	ast.Inspect(file, func(n ast.Node) bool {
		// find function call statements
		// this is a "type assertion"
		func_call, ok := n.(*ast.CallExpr)
		if ok {
			fmt.Println(func_call.Fun)
		}
		return true
	})
}