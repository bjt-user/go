package main

import (
	"time"
)

func main() {
	my_date := time.Now()

	// go has a very weird date layout:
	// 2006 means YYYY
	// 01 means MM
	// 02 means DD
	// you will have to look these codes up every time
	println(my_date.Format("2006-01-02"))
}
