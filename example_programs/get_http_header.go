package main

import (
	"log"
	"net/http"
)

func main() {
	response, err := http.Get("https://example.com/")
	if err != nil {
		log.Fatal(err)
	}

	header := response.Header.Get("server")

	println(header)
}
