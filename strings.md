https://go.dev/ref/spec#String_types

> Strings are immutable: once created, it is impossible to change the contents of a string.

#### string concatenation

```
my_string := "foo"
second_string := my_string + "bar"
```

"extending" the string with `+=`
```
my_string := "foo"
my_string += "bar"
```

#### remove last n chars of a string

this will remove the last 2 characters of a string:
```
my_string := "vim-go"

my_string = my_string[:(len(my_string) - 2)]

fmt.Printf("%s\n", my_string)
```

#### remove first n chars of a string

```
my_string := "vim-go"

fmt.Println(my_string[3:])
```
outputs:
```
-go
```

#### substring (from index to index)

```
my_string := "soup"

fmt.Println(my_string[1:3])
```
outputs:
```
ou
```

#### contains

```
package main

import (
	"fmt"
	"strings"
)

func main() {
	my_string := "vim-go"

	fmt.Println(my_string)

	if strings.Contains(my_string, "v") {
		fmt.Println(my_string, "contains v.")
	}
}
```

#### Trim

```
func Trim(s, cutset string) string
    Trim returns a slice of the string s with all leading and trailing Unicode
    code points contained in cutset removed.
```

```
my_string := "govim-go"

fmt.Println(strings.Trim(my_string, "go"))
```

will print
```
vim-
```

#### TrimSpace (removes all whitespace from start and end)

```
my_string := " \t.RECIPEPREFIX = t	"
my_string = strings.TrimSpace(my_string)

fmt.Printf(">%s<", my_string)
```
outputs:
```
>.RECIPEPREFIX = t<
```

#### Trim To Remove Whitespace (better use TrimSpace)

remove all whitespace from start and end: (BETTER USE TrimSpace)
```
my_string := " vim go	\r\n"

fmt.Printf(">%s<\n", strings.Trim(my_string, " \t\r\n"))
```

#### ReplaceAll

```
my_string := " vim go	\r\n"

fmt.Printf(">%s<\n", strings.ReplaceAll(my_string, " ", ""))
```
will output:
```
>vimgo
<
```

#### Cut

`strings.Cut()` returns 3 values (before string, after string, found bool)

```
my_string := " \t.RECIPEPREFIX = t	"
_, my_string, _ = strings.Cut(my_string, "=")

fmt.Printf(">%s<", my_string)
```
outputs:
```
> t	<
```

#### HasPrefix

```
my_string := "vim-go"

fmt.Println(strings.HasPrefix(my_string, "vim"))
```
will output
```
true
```

#### CutPrefix

```
my_string := ".RECIPEPREFIX = t"
recipeprefix, _ := strings.CutPrefix(my_string, ".RECIPEPREFIX")

fmt.Printf(">%s<", recipeprefix)
```
outputs
```
> = t<
```

#### count substrings

```
my_string := "go is good."

fmt.Println(strings.Count(my_string, "g"))
```
will print
```
2
```

Works for substrings and single characters.

#### multiline strings with backticks

You can initialize multiline strings with backticks:
```
func main() {
	my_string := `ladies and gentlemen,
this is the first line of this letter.

Sincerely,
John Doe`

	fmt.Printf("%s\n(%T)\n", my_string, my_string)
}
```

You can't indent them, because that would be whitespace inside the string.

These types of strings will probably only be useful in unit tests \
and for example code.

## troubleshooting

#### fake strings

Go will print data types as strings that are not strings.

```
fmt.Printf("%v (%T)\n", cexprs[2].Fun, cexprs[2].Fun)
```
This printed:
```
my_cool_func (*ast.Ident)
```

But I had to use a `type assertion` like this:
```
expr_string, ok := cexprs[2].Fun.(*ast.Ident)
```
So that I could use the data type that was recognized as a struct as an actual struct \
and get the underlying field.

#### FAIL: change char of a string

this will not compile:
```
package main

import "fmt"

func change_char(change_me *string) {
	*change_me[3] = 't'
}

func main() {
	my_string := "foobar"
	change_char(&my_string)

	fmt.Println(my_string)
}
```

```
./main.go:6:12: invalid operation: cannot index change_me (variable of type *string)
```

There is no elegant way to do this in Go.\
This is much simpler in C.
