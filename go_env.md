#### print the current go environment

```
go env
```

#### print the GOPATH

```
go env GOPATH
```

#### change the default value of a go env var

```
go env -w GOPATH=/home/me/.golang
```

#### show env vars that have been changed from the default

```
$ go env --changed
GOPATH='/home/me/.golang'
```

#### set default value for a var

This will reset the env var to its default:
```
go env -u GOPATH
```

#### print the config file for the go env

```
$ go env GOENV
/home/me/.config/go/env
```

It only exists if you have actively changed the default settings.
