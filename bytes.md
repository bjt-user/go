#### general

```
$ go doc builtin.byte
package builtin // import "builtin"

type byte = uint8
    byte is an alias for uint8 and is equivalent to uint8 in all ways. It is
    used, by convention, to distinguish byte values from 8-bit unsigned integer
    values.
```

#### convert string to byte slice

```
bs := []byte("vim-go")
```

#### empty byte slice

This is an empty byte slice: `[]byte{}`

Might be useful when returning empty values.
