https://go.dev/tour/moretypes/2

#### example program

```
package main

import "fmt"

type heading struct {
	text  string
	level int
	line  int
}

func main() {
	my_heading := heading{"foo bar", 2, 5}

	fmt.Printf("%s\n", my_heading.text)
	fmt.Printf("%d\n", my_heading.level)
	fmt.Printf("%d\n", my_heading.line)

	// print entire struct
	fmt.Printf("%v\n", my_heading)
}

```

#### slice of structs

```
package main

import "fmt"

type shopping_item struct {
	name     string
	quantity int
	color    string
}

func main() {
	var my_list []shopping_item

	for i := 0; i < 3; i++ {
		my_list = append(my_list, shopping_item{"foo", i, "any"})
	}

	fmt.Printf("%v", my_list)
}
```

This will output:
```
[{foo 0 any} {foo 1 any} {foo 2 any}]
```

So it created a slice of 3 elements all with the same name and color\
but different quantities.

#### print field names

To also print field names and not just the values of an instantiated struct:
```
fmt.Printf("%+v\n", my_struct)
```

#### compare struct objects

"The Go Programming Language" (Alan A. A. Donovan, Brian W. Kernighan): (p104 ch4.4.2 "Comparing Structs")
> If all the fields of a struct are comparable, the struct itself is comparable, \
so two expressions of that type may be compared using `==` or `!=`.

If two struct variables contain the same values \
you should be able to just use the normal comparison operators \
like `==`:
```
type person struct {
	name string
	age  int
}

func main() {
	hugo := person{
		name: "Hugo",
		age:  44,
	}

	fmt.Printf("%+v\n", hugo)

	hugo_clone := person{
		name: "Hugo",
		age:  44,
	}

	fmt.Printf("%+v\n", hugo_clone)

	if hugo == hugo_clone {
		fmt.Println("the clone is equal!")
	}
}
```
outputs:
```
{name:Hugo age:44}
{name:Hugo age:44}
the clone is equal!
```
