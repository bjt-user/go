#### comparing slices

https://pkg.go.dev/slices#Compare

```
first_slice := []string{"foo", "bar"}
second_slice := []string{"foo", "bar"}

fmt.Println(slices.Compare(first_slice, second_slice))
```
will output:
```
0
```

If the slices differ the result is either `1` or `-1`.

> The result of comparing the first non-matching elements is returned.\
If both slices are equal until one of them ends, the shorter slice is considered less than the longer one.\
The result is 0 if s1 == s2, -1 if s1 < s2, and +1 if s1 > s2.

#### sorting a string slice

This will sort the string slice alphabetically.\
(no need to save the return value to a var)

https://pkg.go.dev/sort#Strings
```
sort.Strings(my_slice)
```

> Note: as of Go 1.22, this function simply calls slices.Sort.

So you can also do: (which might be 0.0001% faster)
```
slices.Sort(my_slice)
```

This puts uppercase strings at the top of the list.\
And also strings that start with uppercase.\
To sort case insensitive you probably need your own sort function.

#### slices.Contains()

Very useful function that will check if a value is an element in a slice.

```
my_string_slice := []string{
    "Tom",
    "Carl",
    "Peter",
}

fmt.Println(slices.Contains(my_string_slice, "Frank"))
```
outputs:
```
false
```

But will print `true` if you use a name that is in the slice.

Also works for integers:
```
my_int_slice := []int{
		1,
		2,
		3,
	}

fmt.Println(slices.Contains(my_int_slice, 1))
```
will print `true`

#### contiguous sequence

```
my_strings := []string{"Peter", "Mark", "Travis", "Steve"}

fmt.Println(my_strings)

fmt.Println(my_strings[1:3])
```
outputs
```
[Peter Mark Travis Steve]
[Mark Travis]
```

So the first number of the sequence includes the given element.\
But the last number of the sequence excludes the indexed element.

`[1:3]` -> gives elements 1 and 2

#### pass basic type to a function that expects slice

When you have a function like this:
```
func greet(names []string) {
	fmt.Println("Hello,")
	for _, name := range names {
		fmt.Printf("%s\n", name)
	}
}
```
you are only able to pass a string slice to it.\
Otherwise you will get a compiler error:
```
cannot use my_name (variable of type string) as []string value in argument to greet
```

But you can put your string into a string slice "on the fly" like this:
```
my_name := "Werner"

greet([]string{my_name})
```
and it will work:
```
Hello,
Werner
```

This can be useful if you want to use a function that expects a slice,\
but you only have the corresponding basic type available.

#### pop element at the top

This will insert element `"Robert"` at index 0 \
and shift all other elements up by one:
```
my_slice = slices.Insert(my_slice, 0, "Robert")
```

## delete elements from a slice

#### trying slices.Delete() (WARNING)

This will remove from element 2 ("Steve") until the element 3 (excluding element 3):
```
my_slice := []string{
    "Werner",
    "Hans-Dieter",
    "Steve",
    "Joe",
}

my_slice = slices.Delete(my_slice, 2, 3)

fmt.Printf("%v\n", my_slice)
```
outputs:
```
[Werner Hans-Dieter Joe]
```

WARNING: this function can behave weird inside functions!

#### returning a new slice (preferred way of doing it)

This will work:
```
func remove_peter(names []string) []string {
	names_result := make([]string, 0)

	for _, name := range names {
		if name != "Peter" {
			names_result = append(names_result, name)
		}
	}

	return names_result
}
```

Call by reference will fail because it will not update the length of the slice.

## pitfalls

#### TODO: changing a slice through call by reference

Slices should be reference types so they should use call by reference by default.

But this will not change the slice:
```
func change_strings(names []string) {
	names = []string{
		"foo",
		"bar",
	}
}
```

Why is this not working?

#### iterating over slices while changing the slice

Iterating over a slice while changing the slice inside the loop \
can lead to unexpected behaviour.

This function `filter_names` will remove an element from \
a slice, but the length of the slice will not be removed:
```
func filter_names(names []string) {
	for i, name := range names {
		if name == "Joe" {
			names = slices.Delete(names, i, i+1)
		}
	}
}
```

```
func main() {
	my_slice := []string{
		"Werner",
		"Hans-Dieter",
		"Steve",
		"Joe",
	}

	filter_names(my_slice)

	for i, v := range my_slice {
		fmt.Println(i, v)
	}

	fmt.Printf("len(my_slice): %d\n", len(my_slice))
}
```
outputs:
```
0 Werner
1 Hans-Dieter
2 Steve
3
len(my_slice): 4
```

You can see element 3 is missing, but the length of the slice is still 4.

What would be the correct approach?

Probably not use `slices.Delete()` at all and build a new slice from an existing slice.
