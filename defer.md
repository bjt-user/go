The `defer` keyword executes a function after the current function is done.

Can be useful to close files, streams, do cleanups, etc.\
This way you cannot forget to close a file, because you use the deferred close right after opening a file.

#### example

```
package main

func main() {
        defer println("test")
        println("This is our main function.")
}
```
will output:
```
This is our main function.
test
```