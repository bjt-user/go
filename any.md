`any` is an alias to the empty interface `interface{}`.

```
$ go doc builtin.any
package builtin // import "builtin"

type any = interface{}
    any is an alias for interface{} and is equivalent to interface{} in all
    ways.\
```
