https://go.dev/tour/methods/15

#### purpose

Type assertions are used to assign and assert an underlying concrete type \
to an interface.\
You can use the optional second return value to deal with cases where there \
is another concrete type in the interface that you do not want.
