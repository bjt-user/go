#### os.ReadDir()

```
func ReadDir(name string) ([]DirEntry, error)
    ReadDir reads the named directory, returning all its directory entries
    sorted by filename. If an error occurs reading the directory, ReadDir
    returns the entries it was able to read before the error, along with the
    error.
```

Example:
```
dir_entries, err := os.ReadDir("/tmp")

if err != nil {
    log.Fatal(err)
}

for i, entry := range dir_entries {
    fmt.Printf("%v: %v\n", i, entry)
}
```
outputs:
```
0: d .ICE-unix/
1: - .X0-lock
2: d .X11-unix/
...
```

You can use methods like `IsDir()` on the `DirEntry` objects.

#### list all files in a dir (non-recursive)

```
dir_entries, err := os.ReadDir("/etc")

if err != nil {
    log.Fatal(err)
}

file_counter := 0
for _, entry := range dir_entries {
    if entry.Type().IsRegular() {
        file_counter++
        fmt.Printf("%v: %s\n", file_counter, entry.Name())
    }
}
```

#### IsDir() function

The function is part of the `io/fs` package:\
https://pkg.go.dev/io/fs#FileMode.IsDir

#### check if it is a dir

```
func is_dir(path_to_dir string) (bool, error) {
	file_info, err := os.Stat(path_to_dir)

	if err != nil {
		return false, err
	}

	if file_info.IsDir() {
		return true, nil
	} else {
		return false, nil
	}
}

func main() {
	is_dir, err := is_dir("/temp")

	if err != nil {
		log.Fatal("Function is_dir() returned an error\n", err)
	}

	fmt.Println("is_dir:", is_dir)
}
```

#### simpler check if it is a dir

```
func is_dir(path_to_dir string) bool {
	file_info, err := os.Stat(path_to_dir)

	if err != nil {
		return false
	}

	if file_info.IsDir() {
		return true
	} else {
		return false
	}
}
```

#### assert dir

```
func assert_dir(path_to_dir string) {
	file_info, err := os.Stat(path_to_dir)

	if err != nil {
		log.Fatal(err)
	}

	if !file_info.IsDir() {
		log.Fatal(path_to_dir, " is not a directory.")
	}
}
```
