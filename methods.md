The tour explains methods pretty well:\
https://go.dev/tour/methods/1

> Go does not have classes. However, you can define methods on types.\
A method is a function with a special receiver argument.\
The receiver appears in its own argument list between the func keyword and the method name.

> Remember: a method is just a function with a receiver argument.

Methods will probably mostly be used on structs.\
And they are convenient as you do not have to pass a pointer to it \
if you want to modify the values of the struct.\
You have to specify the receiver as a pointer but you do not need to pass a pointer.\
You can pass either a pointer or the value itself and Go will figure it out for you.

#### call by reference (pointer receivers)

https://go.dev/tour/methods/4

>  This means the receiver type has the literal syntax `*T` for some type T.\
(Also, T cannot itself be a pointer such as `*int`.)

This example works and changes a struct with a method:
```
package main

import (
	"fmt"
	"reflect"
)

type person struct {
	name string
	age  int
}

func (p *person) Advance_in_age() {
	p.age++
}

func main() {
	frank := person{"Frank", 43}

	fmt.Println(reflect.TypeOf(frank))

	fmt.Println(frank.age)

	frank.Advance_in_age()

	fmt.Println(frank.age)
}
```

https://go.dev/tour/methods/8

>  There are two reasons to use a pointer receiver.\
The first is so that the method can modify the value that its receiver points to.\
The second is to avoid copying the value on each method call. This can be more efficient if the receiver is a large struct, for example.

#### multiple methods of the same name

You can definde multiple methods of the same name with different receiver types.\
This is a difference to function which you can only define once.

So this will work:
```
type person struct {
	name string
	age  int
}

type animal struct {
	name string
	age  int
}

func (p person) print_age() {
	fmt.Println(p.age)
}

func (a animal) print_age() {
	fmt.Println(a.age)
}

func main() {
	frank := person{"Frank", 43}

	steven := animal{"Steven", 12}

	frank.print_age()
	steven.print_age()
}
```
while this will NOT work:
```
type person struct {
	name string
	age  int
}

type animal struct {
	name string
	age  int
}

func print_age(p person) {
	fmt.Println(p.age)
}

func print_age(a animal) {
	fmt.Println(a.age)
}

func main() {
	frank := person{"Frank", 43}

	steven := animal{"Steven", 12}

	frank.print_age()
	steven.print_age()
}
```

```
./main.go:21:6: print_age redeclared in this block
	./main.go:17:6: other declaration of print_age
```
