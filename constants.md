https://go.dev/tour/basics/15

Constants can be character, string, boolean, or numeric values.

So slices or arrays can't be constant.

#### example

```
package main

import "fmt"

func main() {
	const message = "hello world"

	fmt.Println(message)
}
```
