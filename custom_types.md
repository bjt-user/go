Custom types will probably mostly used for creating structs.\
A struct is a custom type.

But you can define custom types with any type:
```
type MyFloat float64
```
Now you have a type called `MyFloat` which is essentially a float64.\
I don't know in which situation something like that would actually make sense.
