## exec pkg

#### run shell command

```
package main

import (
        "fmt"
        "os/exec"
)

func main() {
        cmd := exec.Command("ls", "-la")

        out, err := cmd.Output()
        if err != nil {
                fmt.Println("could not run command: ", err)
        }

        fmt.Println("Output: ", string(out))
}
```

WARNING: It is better to use
```
cmd.CombinedOutput()
```
because this will combine stdout and stderr!

#### example that prints command before executing

```
cmd := exec.Command("ansible-playbook", "-v", "mypb.yaml")

fmt.Printf("Command to be executed: %v\n", cmd.String())

out, err := cmd.Output()

if err != nil {
    fmt.Println("could not run command: ", err)
}

fmt.Println("Output: ", string(out))
```

#### set environment for the command

This will NOT work:
```
cmd := exec.Command("GOMODCACHE="+gomodcache, "go", "-C", mod_dir, "get")
```
Because the command will be executed like this:
```
"GOMODCACHE=/my/dir" "go" ...
```

You have to set the environment:
```
cmd := exec.Command("go", "-C", mod_dir, "get")

cmd.Env = append(cmd.Env, "GOMODCACHE="+gomodcache)
```

#### troubleshooting

You have to make sure, that every word in a command is separated \
by a comma, because it will be enclosed by quotes.

See this output:
```
$ go "env GOMOD"
go env GOMOD: unknown command
Run 'go help' for usage.
$ go "env" "GOMOD"
/dev/null
```

So this will fail:
```
cmd := exec.Command("go", "env GOMOD")
```

While this will work:
```
cmd := exec.Command("go", "env", "GOMOD")
```

## os pkg

#### TODO: os.StartProcess()

```
go doc os.StartProcess
```
