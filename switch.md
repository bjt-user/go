https://go.dev/wiki/Switch

> Switches work on values of any type.

> A switch with no value means “switch true”, making it a cleaner version of an if-else chain[...]

#### example: check first character of string

```
package main

import "fmt"

func main() {
	my_string := "-vim-go"

	switch my_string[0] {
	case '#':
		fmt.Printf("its a header\n")
	case '-':
		fmt.Printf("its a list element\n")
	}
}
```

#### else if equivalent

You can use `switch` statements instead of `if ... else if` statements:
```
package main

import (
	"fmt"
	"strings"
)

func main() {
	myString := "```test"

	switch {
	case strings.HasPrefix(myString, "```"):
		fmt.Println("Oh a code block starts or ends")
	case strings.HasPrefix(myString, "#"):
		fmt.Println("this might be a header")
	default:
		fmt.Println("neither a header nor a start or end of code block")
	}
}
```
The `default` is like an `else`.

Might look more readable to use the `switch` statement.

Or another example:
```
mynum := 1

switch {
case mynum == 1:
    fmt.Println("its ONE")
case mynum == 2:
    fmt.Println("two for you")
default:
    fmt.Println("its not one and not two")
}
```
So you can put any conditional statements after the `case` keyword.

#### case with multiple values

```
my_var := "vim-go"

for _, char := range my_var {
    switch char {
    case 'm', 'g':
        fmt.Println("its m or g")
    case 'o':
        fmt.Println("its o")
    }
}
```
outputs:
```
its m or g
its m or g
its o
```

## pitfalls

#### break inside case will not break surrounding for loop

In this program you might expect to break out of the for loop when the rune v is found.\
But this code
```
my_string := "vim-go"

for _, char := range my_string {
    switch char {
    case '-':
        fmt.Println("It's -.")
    case 'v':
        fmt.Println("It's v.")
        break
    }
}
```
outputs:
```
It's v.
It's -.
```

Using a `return` instead of a `break`:
```
for _, char := range my_string {
    switch char {
    case '-':
        fmt.Println("It's -.")
    case 'v':
        fmt.Println("It's v.")
        return
    }
}
```
outputs:
```
It's v.
```
