#### default

Default seems to be `local`.

From
```
go help telemetry
```
> When telemetry is in local mode, counter data is written to the local file
system, but will not be uploaded to remote servers.

This is the default location:\
`~/.config/go/telemetry/local`

#### env vars

```
go env GOTELEMETRY GOTELEMETRYDIR
```

#### turn off telemetry

```
go telemetry off
```
=> this will set the go env vars as well
