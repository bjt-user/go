https://go.dev/tour/basics/1

#### can't run without package

A Go program needs a package to run.

A program like this will not compile:
```
import "fmt"

func main() {
	fmt.Println("vim-go")
}
```

```
$ go run main.go
main.go:1:1: expected 'package', found 'import'
```

#### run a program with multiple files

Lets say you have a file `foo.go`
```
package main

import "fmt"

func foo() {
	fmt.Println("foo")
}
```

and a file called `main.go` with this content:
```
package main

func main() {
	foo()
}
```

You can run your package `main` by running both files:
```
go run main.go foo.go
```
or
```
go run foo.go main.go
```
So the order of file names does not seem to matter.

#### working with multiple packages

It seems that your program can not work with multiple packages.

If you have these two files with different packages:
```
package main

func main() {
	helper.greet("Joe")
}
```

```
package helper

import "fmt"

func greet(name string) {
	fmt.Println("Hello,", name)
}
```
you get this error when running:
```
$ go run main.go helper.go
found packages main (main.go) and helper (helper.go) in /home/myuser/coding/go/multiple_packages
```

#### libraries

> The go build command compiles each argument package.\
If the package is a library, the result is discarded; this merely checks \
that the package is free of compile errors.\
If the package is named `main`, `go build` invokes the linker to create an \
executable in the current directory[...]
"The Go Programming Language" (Alan A. A. Donovan, Brian W. Kernighan) (p293 ch 10.7.3)

Running a `go build` on a file that is of `package foo` will not produce any executables.

#### running a program without a main package

It seems like your program needs to be part of the main package to even run:
```
$ go run main.go
package command-line-arguments is not a main package
```
