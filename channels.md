https://go.dev/tour/concurrency/2

#### declare a channel

```
var my_channel chan int

fmt.Printf("%T, %v\n", my_channel, my_channel)
```
will output:
```
$ go run main.go
chan int, <nil>
```

or do: (this is the way they do it in the docs)
```
ch := make(chan int)
```

With `make` the channel has a hex value:
```
my_channel := make(chan int)

fmt.Printf("%T, %v\n", my_channel, my_channel)
```

```
$ go run main.go
chan int, 0xc0000200c0
```

#### make

From
```
go doc builtin.make
```
> Channel: The channel's buffer is initialized with the specified
 buffer capacity. If zero, or the size is omitted, the channel is
 unbuffered.
