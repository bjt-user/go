package main

import (
	"go/ast"
	"go/parser"
	"go/token"
	"log"
)

// arg1: every string in the slice represents the source code of a go file
// returns: the parsed and resulting ast.Files and the FileSet
// note that this function will not give file names to the files
// it is probably more of a convenience function for tests
func strings_to_ast(srcs []string) ([]ast.File, *token.FileSet) {
	ast_files := make([]ast.File, 0)

	fset := token.NewFileSet()

	for _, src_file := range srcs {
		af, err := parser.ParseFile(fset, "", src_file, parser.SkipObjectResolution)
		if err != nil {
			log.Fatalf("parser.ParseFile failed.\n%s", err)
		}
		ast_files = append(ast_files, *af)
	}

	return ast_files, fset
}

// Argument 1: A map where the first string is the name of a source file
// and the second string is the source code of the Go file.
// With the file names available in the resulting FileSet this function will be more useful
// than functions that just take a string slice.
func strmap_to_ast(srcs map[string]string) ([]ast.File, *token.FileSet) {
	ast_files := make([]ast.File, 0)

	fset := token.NewFileSet()

	for file_name, file_content := range srcs {
		af, err := parser.ParseFile(fset, file_name, file_content, parser.SkipObjectResolution)
		if err != nil {
			log.Fatalf("parser.ParseFile failed.\n%s", err)
		}
		ast_files = append(ast_files, *af)
	}

	return ast_files, fset
}

// returns slice of pointer to ast.File instead of raw ast.File
// also returns an error instead of just doing a log.Fatal
// which makes the function more testable
func strmap_to_astp(srcs map[string]string) ([]*ast.File, *token.FileSet, error) {
	ast_files := make([]*ast.File, 0)

	fset := token.NewFileSet()

	for file_name, file_content := range srcs {
		af, err := parser.ParseFile(fset, file_name, file_content, parser.SkipObjectResolution)
		if err != nil {
			return nil, nil, err
		}
		ast_files = append(ast_files, af)
	}

	return ast_files, fset, nil
}
