#### declaring multiple global variables

```
package main

import "fmt"

var (
	foo string = "bar"
	bar int    = 1
)

func main() {
	fmt.Println(foo)
	fmt.Println(bar)
}
```
