#### compiler

By default the `go` tool uses the `gc` (go compiler).\
I was not able to find a `gc` binary in `GOROOT` or `GOPATH` however.

You can also use `gccgo` by using the `-compiler` flag:
```
-compiler name
		name of compiler to use, as in runtime.Compiler (gccgo or gc).
```

#### -x (print commands)

> print the commands.

```
go build -x myprogram.go
```

#### -gcflags

View all possible `-gcflags`:
```
go tool compile -help
```

This seems to be better that just using `-trimpath` \
because it keeps your `GOROOT` intact:
```
go build -gcflags=-trimpath="$(pwd)" main.go
```

#### -trimpath

For production code you might want to use this flag:
```
go build -trimpath
```

Then when panics occur, it is not the path of your workstation that is shown.\
Users will otherwise know the absolute path of the dir you have built the binary in.

#### linker flags

You can pass variables through the linker:
```
go build -ldflags="-X 'main.version=v1.0.0'" main.go
```

But this will only work if `version` is a GLOBAL variable in your `main` package.

#### print temporary work dir

```
go build -work main.go
```
