You may break the line after a `(`:
```
response, err := http.Get(
		"https://example.com" + MY_PATH_VAR)
```

You may break the line after a `+`:
```
full_url := BASE_URL +
	"?apiKey=" +
	API_TOKEN
```

You may break the line after a `.`:
```
	if err != nil {
		log.
			Fatal(err)
	}
```
But that seems very ugly.

#### continuing a string

This will NOT work:
```
func main() {
        myvar := "foo \
         bar"

        fmt.Println(myvar)
}
```

For variables you could concatenate the strings like this:
```
myvar := "foo"
myvar += "bar"

fmt.Println(myvar)
```

#### continuing a string in fmt print functions

```
fmt.Println("hello this is a very " +
		"long string ok and it goes over several " +
		"lines.")
```
