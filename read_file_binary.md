This reads a file and shows its binary hex code:
```
package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	content, err := os.ReadFile("myfile.txt")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%x\n", content)
}
```

It shows the numbers as
```
xxd myfile.txt
```
but all numbers are in the same line\
and the hex blocks are not separated by new lines.

#### separate hex blocks by new lines

This will print 8 bytes per line:
```
package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	bytes, err := os.ReadFile("myfile.txt")

	if err != nil {
		log.Fatal(err)
	}

	for index, value := range bytes {
		if index%8 == 0 {
			fmt.Printf("\n")
		}
		fmt.Printf("%02x ", value)
	}
}
```

By default `xxd` groups two bytes and outputs 16 bytes per line.\
One character is one byte according to ascii https://www.ascii-code.com/

The output from the go program above is similar to a
```
xxd -g1 -c8 myfile.txt
```

```
-c cols | -cols cols
      Format  <cols>  octets  per line. Default 16
```
