https://go.dev/doc/install

#### arch linux

```
sudo pacman -S go
```

#### Ubuntu

Try manual installation since the Ubuntu package is very old.

Uninstall old packages:
```
sudo apt purge golang
```

```
sudo apt autoremove
```

https://go.dev/dl/

Download the Linux AMD64 version.

Remove old installation and extract the go tar.gz:
```
sudo rm -vrf /usr/local/go && sudo tar -C /usr/local -xvzf go1.22.2.linux-amd64.tar.gz
```

Expand the PATH by putting this in `.bashrc`:
```
# ubuntu - manual go installation
if [[ $(head -1 /etc/os-release) =~ "Ubuntu" ]]; then
	export PATH=$PATH:/usr/local/go/bin
fi
```

Verify the version:
```
$ go version
go version go1.22.2 linux/amd64
```
