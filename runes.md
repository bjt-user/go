#### what is a rune

https://go.dev/blog/strings

> Go source code is always UTF-8.\
A string holds arbitrary bytes.\
A string literal, absent byte-level escapes, always holds valid UTF-8 sequences.\
Those sequences represent Unicode code points, called runes.\
No guarantee is made in Go that characters in strings are normalized.

#### initializing a rune

```
package main

import "fmt"

func main() {
	var my_rune rune = 'r'

	fmt.Println(my_rune)
}
```
will output
```
114
```

You need to use the `%c` formatter:
```
var my_rune rune = 'r'

fmt.Printf("%c\n", my_rune)
```

#### a rune is an int32 under the hood

```
var my_rune rune = 'r'
my_char := 'x'

fmt.Printf("%c\n", my_rune)
fmt.Printf("%c\n", my_char)
fmt.Printf("%T\n", my_rune)
fmt.Printf("%T\n", my_char)
```
will output:
```
r
x
int32
int32
```

#### empty rune literals

A rune literal may not be empty:
```
var my_rune rune = ''

fmt.Printf("%c\n", my_rune)
```
will output
```
./main.go:6:22: empty rune literal or unescaped '
```

Setting the value `0` will work:
```
var my_rune rune = 0

fmt.Printf(">%c<\n", my_rune)
```
will output:
```
><
```

#### check for whitespace

https://pkg.go.dev/unicode#IsSpace

```
IsSpace reports whether the rune is a space character as defined by Unicode's White Space property; in the Latin-1 space this is

'\t', '\n', '\v', '\f', '\r', ' ', U+0085 (NEL), U+00A0 (NBSP).
```

```
my_string := " vim-go"

fmt.Printf("%T\n", my_string[0])
fmt.Printf("%T\n", rune(my_string[0]))

fmt.Printf("%v\n", unicode.IsSpace(rune(my_string[0])))
```

outputs:
```
uint8
int32
true
```

```
my_string := "\tvim-go"

fmt.Printf("%T\n", my_string[0])
fmt.Printf("%T\n", rune(my_string[0]))

fmt.Printf("%v\n", unicode.IsSpace(rune(my_string[0])))
```

outputs:
```
uint8
int32
true
```

Pressing tab at the beginning of the string is also detected as whitespace.\
`\n` is also detected as whitespace.

#### iterate over string rune by rune

this worked:\
https://stackoverflow.com/questions/18130859/how-can-i-iterate-over-a-string-by-runes-in-go

this also works:
```
my_string := "foobar"

i := 0

for i < len(my_string) {
    fmt.Printf("%c\n", my_string[i])
    i++
}
```

#### comparing runes

Since runes are have an integer value and the rune value they can be checked in two ways.

A tab can be checked by its numerical value 9 or by the tab character/rune:
```
my_rune := '\t'

if my_rune == 9 {
    fmt.Println("Your rune is a tab.")
}

if my_rune == '\t' {
    fmt.Println("Your rune is a tab.")
}
```
This will output:
```
Your rune is a tab.
Your rune is a tab.
```

#### cast string to rune

You cant just do
```
rune(my_string)
```
even if the string only contains one rune.\
You have to select one rune of the string like this:
```
rune(my_string[0])
```

This is how I do it:
```
one_char_string := "v"
var converted_rune rune = 0

if len(one_char_string) == 1 {
    converted_rune = rune(one_char_string[0])
}

fmt.Printf("%T\n", one_char_string)
fmt.Printf("%T\n", converted_rune)

fmt.Printf("%c\n", one_char_string[0])
fmt.Printf("%c\n", converted_rune)
```
will output:
```
string
int32
v
v
```

#### append rune to string

```
my_string := "vim-go"

my_rune := 'd'

god := my_string + string(my_rune)

fmt.Println(god)
```
outputs
```
vim-god
```
