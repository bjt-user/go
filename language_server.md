gopls is the official language server

https://pkg.go.dev/golang.org/x/tools/gopls#section-readme

#### installation (vim)

install language server
```
sudo pacman -S gopls
```

install vim plugin `vim-go`
```
git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go
```
(this will start a process of the `gopls` binary)\
(the process will time out and terminate itself when it does not receive requests for 1 minute)

Open a `.go` file in vim.

Type <kbd>ctrl</kbd> <kbd>x</kbd> then <kbd>ctrl</kbd> <kbd>o</kbd> when you want code completion to show up.
