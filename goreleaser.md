#### installation

```
sudo pacman -S goreleaser
```

#### usage
```
goreleaser init
```

change .goreleaser.yaml

You can remove `windows` and `darwin` from \
builds -> goos if you do not support those

```
export GITHUB_TOKEN=...
```

```
git tag v0.0.1 -m release
```

```
goreleaser release --clean
```

#### workflow

For bigger projects it might make sense to have a version flag.

1. (update the hardcoded version flag in your source code)
1. commit/merge into main
1. `git tag v0.0.1 -m release`
1. `export GITHUB_TOKEN=...`
1. `goreleaser release --clean`

#### do not archive

To upload the binary directly without putting the in a `.tar.gz`:
```
archives:
  - format: binary
```

#### dryrun

```
goreleaser release --snapshot --clean
```

Then you can see the builds in `dist`.\
And run them like this:
```
./dist/myprogram_linux_amd64_v1/myprogram
```

#### see build command

`--verbose`

#### custom name for uploaded assets

This way you can leave out the version so that you can use Github's latest link for example:
```
archives:
  - format: binary

    name_template: "{{ .ProjectName }}_{{ .Os }}_{{ .Arch }}"
```

#### build flags

```
builds:
  - env:
      - CGO_ENABLED=0
    goos:
      - linux
    flags:
      - -trimpath
```

#### ldflags

https://goreleaser.com/cookbooks/using-main.version/

> By default, GoReleaser will set the following 3 ldflags:

- main.version: Current Git tag (the v prefix is stripped) or the name of the snapshot, if you're using the --snapshot flag
- main.commit: Current git commit SHA
- main.date: Date in the RFC3339 format

> You can use them in your main.go file to print more build details:

```
package main

import "fmt"

var (
    version = "dev"
    commit  = "none"
    date    = "unknown"
)

func main() {
  fmt.Printf("my app %s, commit %s, built at %s", version, commit, date)
}
```

## troubleshooting

#### shell commands

You cant seem to just use shell commands.\
You need to source an external script or something along those lines.

#### double quotes

You can't place double quotes around goreleaser vars.\
Varibles or "name templates" will not expand then.

Wrong:
```
"{{ .Env.PWD }}"`
```

Correct:
```
{{ .Env.PWD }}
```
