#### installation

```
git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go
```

```
sudo pacman -S gopls
```


#### features

- code completion (when `gopls` is installed on the system) \
  with <kbd>ctrl</kbd> + <kbd>x</kbd>, then <kbd>ctrl</kbd> + <kbd>o</kbd>

- automatically writes `import` statements on save for used packages if you forgot those

- go to definition on <kbd>ctrl</kbd> + <kbd>]</kbd> (like ctags) \
(this works even for third party libraries that you got with `go get` and builtin functions)

- provides documentation with <kbd>shift</kbd> + <kbd>k</kbd> for functions,
  packages (works only in the import section at the start of the program), etc

- Vim-go formats Go code with the formatter of your choice (see :help g:go_fmt_command) on save

