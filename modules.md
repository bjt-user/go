https://go.dev/blog/using-go-modules

https://go.dev/wiki/GOPATH

#### what is a module

> A module is a collection of Go packages stored in a file tree with a go.mod file at its root.\
The go.mod file defines the module’s module path, which is also the import path used for the root directory,\
and its dependency requirements, which are the other modules needed for a successful build.\
Each dependency requirement is written as a module path and a specific semantic version.

#### show the go.mod file of the module you are in

Even if you are in a nested subdir, this command will show the go.mod file:
```
go env GOMOD
```

Use different dir than `pwd`:
```
go -C ~/workspace/git/third_party/golang/tools/cmd/ env GOMOD
```

#### GOPATH mode and module mode

There seems to be a distinction between "GOPATH mode" and "module mode".

> Starting in Go 1.13, module mode will be the default for all development.

#### print dependencies

Inside a module execute this command:
```
go get -x
```
This will show URLs that link to version numbers.\
But maybe you can get to the source code from there.

#### temporary setting GOPATH

When inside a module you can temporarily set the GOPATH:
```
export GOPATH="$(pwd)"
```

Then when you
```
go get -x
```
you pull all the dependencies in the current dir.

## troubleshooting

#### no package to get in current directory

You can have `go.mod` files in dirs, but have the Go files in subdirs.

There is no package at the top level of `golang.org/x/tools`.

To fetch all the dependencies of all the packages in x/tools, use
```
go get ./...
```
