https://pkg.go.dev/log

It formats output with timestamps.\
You can also pass errors to it with `log.Fatal(err)`\
(that will also exit the program)

> The Fatal functions call os.Exit(1) after writing the log message.\
The Panic functions call panic after writing the log message.

> It also has a predefined 'standard' Logger accessible through \
helper functions Print[f|ln], Fatal[f|ln], and Panic[f|ln], which are easier to use than creating a Logger manually.

So for custom formatting it looks like you have to create a "Logger".

#### example program: print simple info message

```
package main

import "log"

func main() {
	log.Println("This is a test.");
}
```
