package http // import "net/http"

Package http provides HTTP client and server implementations.

#### http.Client (struct)

`go doc http.Client`:
> A Client is an HTTP client. Its zero value (DefaultClient) is a usable \
client that uses DefaultTransport.

From `go doc http.Get`:
> Get is a wrapper around DefaultClient.Get.

> To make a request with custom headers, use NewRequest and DefaultClient.Do.

#### http.Request (struct)

```
go doc http.Request
```

You should be able to create custom headers with this struct.
