#### how to get a url.URL

```
my_url, err := url.Parse("http://example.com")

if err != nil {
        panic(err)
}

fmt.Printf("%#v\n", my_url)
```
