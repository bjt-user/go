HTTP headers are just suggestions from the client, and the server has full control over whether to honor them.

```
$ go doc http.Header
package http // import "net/http"

type Header map[string][]string
```

#### working example

Since a `http.Header` is just a `map[string][]string`, this works:
```
var my_header http.Header = map[string][]string{
        "User-Agent": []string{"go-client"},
}

my_header.Add("foo", "bar")

fmt.Printf("%#v\n", my_header)
```

#### FAIL

```
var my_header http.Header

my_header.Add("User-Agent", "go-client")

fmt.Printf("%#v\n", my_header)
```
outputs:
```
panic: assignment to entry in nil map

goroutine 1 [running]:
net/textproto.MIMEHeader.Add(...)
        /usr/lib/go/src/net/textproto/header.go:15
net/http.Header.Add(...)
        /usr/lib/go/src/net/http/header.go:31
main.main()
        /home/bofo/coding/go/net/http/header/main.go:11 +0x39
exit status 2
```
