#### get http proxy url from environment

This works, but you need `HTTP_PROXY`, `HTTPS_PROXY`, and `NO_PROXY` \
to be exported:
```
dest_url, err := url.Parse("http://example.com")

if err != nil {
        panic(err)
}

var my_request http.Request

my_request.Method = "HEAD"
my_request.URL = dest_url

if &my_request == nil {
        panic("Your request is nil???")
}

proxy_url, err := http.ProxyFromEnvironment(&my_request)

if err != nil {
        panic(err)
}

if proxy_url != nil {
        fmt.Printf("proxy_url: %v\n", proxy_url)
}
if dest_url != nil {
        fmt.Printf("dest_url: %v\n", dest_url)
}
```
