HTTP headers are just suggestions from the client, and the server has full control over whether to honor them.

#### example with custom headers

```
package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {
	req, err := http.NewRequest("GET", "http://example.com", nil)

	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Cache-Control", "no-cache")

	fmt.Printf("%#v\n", *req)

	resp, err := http.DefaultClient.Do(req)

	defer resp.Body.Close()

	resp_body, err := io.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	fmt.Println("resp.Status:", resp.Status)
	fmt.Println("resp.Header:", resp.Header)

	fmt.Printf("resp_body:\n%v\n", string(resp_body))
}
```
