https://pkg.go.dev/flag

#### drawback

From the docs:
> Flag parsing stops just before the first non-flag argument ("-" is a non-flag argument) or after the terminator "--".


So this `-V` will not be recognized:
```
$ ./main nonflag -V
```

```
$ ./main -V
Version 1
```

Flags that are passed after the first non-flag will not be recognized as a flag:
```
$ ./main foo -V foo
flag.Args(): []string{"foo", "-V", "foo"}
flag.NArg(): 3
flag.NFlag(): 0
```

#### flag.Args

You can use this function after the flags have been parsed \
to get cli arguments that are not flags.

```
func Args() []string
    Args returns the non-flag command-line arguments.
```

Example:
```
func get_cli_args() cli_args {
	args := cli_args{}

	flag.BoolVar(&args.version, "v", false, "print version")
	flag.BoolVar(&args.version, "version", false, "print version")
	flag.Parse()

	non_flag_args := flag.Args()

	if len(non_flag_args) == 1 {
		args.file_path = non_flag_args[0]
	} else {
		args.file_path = ""
	}

	return args
}
```

Warning: This will not work if the non-flag arg is passed before the flag argument.

#### Narg()

```
func NArg() int
    NArg is the number of arguments remaining after flags have been processed.
```

#### custom usage

Define a normal usage function somewhere in your package:
```
func usage() {
	fmt.Printf("Usage:\n")
}
```

Then assign it to `flag.Usage` before `flag.Parse()`:
```
flag.Usage = usage

flag.Parse()
```
