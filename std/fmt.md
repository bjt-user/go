## Fprintf

```
myvar := "foo"
fmt.Fprintf(os.Stdout, "%s\n", myvar)
```
outputs:
```
foo
```

#### Example when writing to a file
```
myvar := "foo"
myfile, err := os.OpenFile("foo.txt", os.O_RDWR|os.O_CREATE, 0644)

if err != nil {
    log.Fatal(err)
}

file_writer := bufio.NewWriter(myfile)

fmt.Fprintf(file_writer, "%s\n", myvar)

file_writer.Flush() // Don't forget to flush!

myfile.Close()
```

#### example writing to strings.Builder
```
var sbuilder strings.Builder

fmt.Fprintf(&sbuilder, "foo")

fmt.Printf("%+v\n", &sbuilder)
```

## Sprintf

```
func Sprintf(format string, a ...any) string
```

> Sprintf formats according to a format specifier and returns the resulting string.

`Sprintf` seems to be very useful when converting data types into strings.

Types like structs or interfaces can provide an output when using `fmt.Printf("%v", myvar)`.\
But you cannot compare that output directly with a string.

For example if you want to compare the output of an interface to a string:
```
if "&{fmt Println}" != fmt.Sprintf("%s", cexprs[0].Fun) {
    t.Fatalf("Callexpression does not match!\n")
}
```

## formatting

https://pkg.go.dev/fmt#hdr-Printing

#### %q (quoted string)

```
my_string := "foo"
fmt.Printf("This is a quoted string: %q\n", my_string)
```
outputs:
```
This is a quoted string: "foo"
```

#### %p (pointer)

```
my_string := "foo"

p_my_string := &my_string

fmt.Printf("%s -> %s\n", *p_my_string, my_string)
fmt.Printf("%p -> %s\n", p_my_string, my_string)
fmt.Printf("%p -> %p\n", p_my_string, &my_string)
```
outputs:
```
foo -> foo
0xc0000a4050 -> foo
0xc0000a4050 -> 0xc0000a4050
```

#### %#v

The Go documentations says:\
> %#v	a Go-syntax representation of the value

examples:
```
type person struct {
	age  int
	name string
}

func main() {
	my_string := "foo"
	my_string_p := &my_string
	my_int := 3

	carl := person{
		age:  99,
		name: "Carl",
	}

	fmt.Printf("my_string is %#v\n", my_string)
	fmt.Printf("my_int is %#v\n", my_int)
	fmt.Printf("my_string_p is %#v)\n", my_string_p)
	fmt.Printf("my_string_p is %p)\n", my_string_p)
	fmt.Printf("carl is %#v\n", carl)
}
```
outputs:
```
my_string is "foo"
my_int is 3
my_string_p is (*string)(0xc0000140a0))
my_string_p is 0xc0000140a0)
carl is main.person{age:99, name:"Carl"}
```

Strings are printed as quoted strings.\
Integers are printed as normal integers.\
Pointers have the data type before the pointer.\
Struct objects have the struct name in front of them and the field names.\
(more verbose than `%+v`)
