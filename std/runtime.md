#### GOROOT()

```
func GOROOT() string
```
GOROOT returns the root of the Go tree.\
It uses the GOROOT environment variable, if set at process start, or else the root used during the Go build.
