From the docs:

```
type ReadCloser

type ReadCloser interface {
	Reader
	Closer
}

ReadCloser is the interface that groups the basic Read and Close methods. 
```

#### implemented by

An `os.File` implements `io.ReadCloser` because it has these methods:
```
func (f *File) Close() error
func (f *File) Read(b []byte) (n int, err error)
```
