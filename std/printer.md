https://pkg.go.dev/go/printer

> Package printer implements printing of AST nodes.

## Fprint

https://pkg.go.dev/go/printer#Fprint

> Fprint "pretty-prints" an AST node to output.

You will first need to create a `FileSet` and parse a file,\
then get your nodes.

#### example 1
```
package main

import (
	"go/parser"
	"go/printer"
	"go/token"
	"log"
	"os"
)

func main() {
	src := `package main
func main() {
	fmt.Println("Hello world.")

	my_cool_func("ok")
}`

	fset := token.NewFileSet()

	file_node, err := parser.ParseFile(fset, "", src, parser.SkipObjectResolution)

	if err != nil {
		log.Fatal(err)
	}

	printer.Fprint(os.Stdout, fset, file_node)
}
```
