#### build.Context (struct)
```
go doc go/build.Context
```

Contains these fields:
```
GOROOT string // Go root
GOPATH string // Go paths
```

#### build.Default

```
default_context := build.Default

fmt.Printf("%#v\n", default_context)
```
will print `GOROOT`, `GOPATH` and so on.

```
$ go doc build.Default
package build // import "go/build"

var Default Context = defaultContext()
    Default is the default Context for builds. It uses the GOARCH, GOOS, GOROOT,
    and GOPATH environment variables if set, or else the compiled code's GOARCH,
    GOOS, and GOROOT.
```
