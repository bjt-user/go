https://pkg.go.dev/go/token

#### create new FileSet

```
func NewFileSet() *FileSet
```

#### FileSet method: Position

Makes a `token.Pos` way more readable \
since it includes `Filename`, `Line` and `Column` information.
```
func (s *FileSet) Position(p Pos) (pos Position)
    Position converts a Pos p in the fileset into a Position value. Calling
    s.Position(p) is equivalent to calling s.PositionFor(p, true).
```

Since a `token.Pos` is an integer you can pass and integer.\
Passing 0 will return an invalid Position.\
Passing an integer that is outside the FileSet will also return an invalid Position.

#### token.Pos

```
type Pos int
```

methods:
```
func (p Pos) IsValid() bool
```

#### type token.Position

```
type Position struct {
	Filename string // filename, if any
	Offset   int    // offset, starting at 0
	Line     int    // line number, starting at 1
	Column   int    // column number, starting at 1 (byte count)
}
```

Offset is the number of the character in the file, start counting at 0.\
Line is the line number in the source file, start counting at 1.\
Column is the character position on the line, start counting at 1.

When the second file in the FileSet is reached, all three integers start counting from the \
beginning again.\
And the only way to know in which file you are is the `Filename` string.

#### type token.Token

```
$ go doc token.Token
package token // import "go/token"

type Token int
    Token is the set of lexical tokens of the Go programming language.

const ILLEGAL Token = iota ...
func Lookup(ident string) Token
func (tok Token) IsKeyword() bool
func (tok Token) IsLiteral() bool
func (tok Token) IsOperator() bool
func (op Token) Precedence() int
func (tok Token) String() string
```

#### Lookup

```
func Lookup(ident string) Token
```

```
my_token := token.Lookup("struct")

println(my_token.IsKeyword())
println(my_token.IsLiteral())
println(my_token.IsOperator())
println(my_token.Precedence())
```
outputs:
```
true
false
false
0
```

#### TODO: FileSet.AddFile

https://pkg.go.dev/go/token#FileSet.AddFile
