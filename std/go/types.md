#### docs

https://github.com/golang/example/tree/master/gotypes

#### about

This package is only useful when analyzing Go source code.\
For instance when writing helper tools for Go or IDE plugins.

```
go doc types
```

#### initializing

First you need to create a `types.Config` object:
```
conf := types.Config{Importer: importer.Default()}
```

Then you use the `Check` method on it:
```
func (conf *Config) Check(path string, fset *token.FileSet, files []*ast.File, info *Info) (*Package, error)
```

> Check type-checks a package and returns the resulting package object and the \
first error if any. Additionally, if info != nil, Check populates each of \
the non-nil maps in the Info struct.

So you might be able to construct a `types.Info` object and then pass it to the `Check` function.

#### NewChecker() and types.checker.files

> Alternatively, create a new type checker with NewChecker and invoke it
incrementally by calling Checker.Files.

types.NewPackage() (expects path to Go files and package name) \
and returns types.Package.

Then you can use NewChecker on the Package. \
It will return a types.Checker.

Then you can run Checker.Files.

But it also cant find imports of external packages.

## structs

#### types.Info struct

```
go doc go/types.Info
```

> The two fields of type `map[*ast.Ident]Object` are the most important:\
`Defs` records declaring identifiers and `Uses` records referring identifiers.

#### types.Scope

```
type Scope struct {
	parent   *Scope
	children []*Scope
	number   int               // parent.children[number-1] is this scope; 0 if there is no parent
	elems    map[string]Object // lazily allocated
	pos, end token.Pos         // scope extent; may be invalid
	comment  string            // for debugging only
	isFunc   bool              // set if this is a function scope (internal use only)
}
```

#### types.Package

```
type Package struct {
	path      string
	name      string
	scope     *Scope
	imports   []*Package
	complete  bool
	fake      bool   // scope lookup errors are silently dropped if package is fake (internal use only)
	cgo       bool   // uses of this package will be rewritten into uses of declarations from _cgo_gotypes.go
	goVersion string // minimum Go version required for package (by Config.GoVersion, typically from go.mod)
}
```

## functions

#### types.NewPackage()

First argument is the path, can be an empty string.

Second argument is the package name, can be an empty string.\
The package name must match the package name of the parsed files \
or be an empty string.

#### types.Config.Check()

```
$ go doc types.Config.Check
package types // import "go/types"

func (conf *Config) Check(path string, fset *token.FileSet, files []*ast.File, info *Info) (*Package, error)
    Check type-checks a package and returns the resulting package object and the
    first error if any. Additionally, if info != nil, Check populates each of
    the non-nil maps in the Info struct.

    The package is marked as complete if no errors occurred, otherwise it is
    incomplete. See [Config.Error] for controlling behavior in the presence of
    errors.

    The package is specified by a list of *ast.Files and corresponding file set,
    and the package path the package is identified with. The clean path must not
    be empty or dot (".").
```

First argument `path` (string) is:
> the package path the package is identified with

The "package path" might be a github URL where the go package resides.\
Or a part of the URL.

You are passing a slice of `*ast.File`s and every ast file can have another package.\
So you have to make sure that all the ast files in the slice are of the same package.

You can get the package name of an ast file through `my_ast_file.Name.String()`.\
But that will probably only be the "package path" if it is a local package.

From the tutorial:
> Since not all facts computed by the type checker are needed by every client,\
the API lets clients control which components of the result should be recorded and which discarded:\
only fields that hold a non-nil map will be populated during the call to Check.


#### TODO: try out these functions

```
func go/types.CheckExpr(fset *FileSet, pkg *types.Package, pos Pos, expr ast.Expr, info *types.Info) (err error)
func go/types.Eval(fset *FileSet, pkg *types.Package, pos Pos, expr string) (_ types.TypeAndValue, err error)
func go/types.(*Scope).LookupParent(name string, pos Pos) (*types.Scope, types.Object)
```

For the third function you should be able to just use the `scope` field of a `types.Package` \
which you get from the `Check` function.

#### helper function

I wrote this helper function for local packages.\
It uses the name of the package the ast.File is in as "package path".

```
// Uses the package name of each ast.File as first argument of the Check
// function. ("package path")
// So this function should be used for local packages.
func get_type_info(fset *token.FileSet, afs []*ast.File) (*types.Info, error) {
	info := &types.Info{
		Defs: make(map[*ast.Ident]types.Object),
		Uses: make(map[*ast.Ident]types.Object),
	}

	conf := types.Config{Importer: importer.Default()}

	for _, af := range afs {
		_, err := conf.Check(af.Name.String(), fset, afs, info)

		if err != nil {
			return nil, err
		}
	}

	return info, nil
}
```

## examples

#### 1 - first working example

This works and the information that `my_string` in the source strings is \
of type string comes out of the `types.Info` object:\
(in the `Uses` and `Defs` Fields)
```
package main

import (
	"fmt"
	"go/ast"
	"go/importer"
	"go/parser"
	"go/token"
	"go/types"
	"log"
)

func strings_to_ast(srcs []string) ([]*ast.File, *token.FileSet) {
	ast_files := make([]*ast.File, 0)

	fset := token.NewFileSet()

	for _, src_file := range srcs {
		af, err := parser.ParseFile(fset, "", src_file, parser.SkipObjectResolution)
		if err != nil {
			log.Fatalf("parser.ParseFile failed.\n%s", err)
		}
		ast_files = append(ast_files, af)
	}

	return ast_files, fset
}

func main() {
	srcs := make([]string, 2)

	srcs[0] = `package main

func main() {
	my_string := "foo"
	hello(my_string)
}`

	srcs[1] = `package main

import (
	"fmt"
)

func hello(greetstr string) {
	fmt.Println("hello", greetstr)
}`

	conf := types.Config{Importer: importer.Default()}

	afs, fset := strings_to_ast(srcs)

	info := &types.Info{
		Defs: make(map[*ast.Ident]types.Object),
		Uses: make(map[*ast.Ident]types.Object),
	}

	_, err := conf.Check("check_path", fset, afs, info)

	if err != nil {
		log.Fatal(err)
	}

	if info != nil {
		fmt.Printf("%+v (%T)\n", info.Defs, info.Defs)
		fmt.Printf("%+v (%T)\n", info.Uses, info.Uses)
	} else {
		fmt.Println("info is nil.")
	}
}
```

#### 2 - improved example with Position values

```
package main

import (
	"fmt"
	"go/ast"
	"go/importer"
	"go/parser"
	"go/token"
	"go/types"
	"log"
)

func strmap_to_ast(srcs map[string]string) ([]*ast.File, *token.FileSet, error) {
	ast_files := make([]*ast.File, 0)

	fset := token.NewFileSet()

	for file_name, file_content := range srcs {
		af, err := parser.ParseFile(fset, file_name, file_content, parser.SkipObjectResolution)
		if err != nil {
			return nil, nil, err
		}
		ast_files = append(ast_files, af)
	}

	return ast_files, fset, nil
}

// Uses the package name of each ast.File as first argument of the Check
// function. ("package path")
// So this function should be used for local packages.
func get_type_info(fset *token.FileSet, afs []*ast.File) (*types.Info, error) {
	info := &types.Info{
		Defs: make(map[*ast.Ident]types.Object),
		Uses: make(map[*ast.Ident]types.Object),
	}

	conf := types.Config{Importer: importer.Default()}

	for _, af := range afs {
		_, err := conf.Check(af.Name.String(), fset, afs, info)

		if err != nil {
			return nil, err
		}
	}

	return info, nil
}

func main() {
	srcs := make(map[string]string, 2)

	srcs["main.go"] = `package main

func main() {
	my_args := cli_args{
		true,
	}
	my_args.evaluate()
}`

	srcs["cli_args.go"] = `package main

import (
	"fmt"
)

type cli_args struct {
	verbose bool
}

func (args cli_args)evaluate() {
	if args.verbose {
		fmt.Println("Lets be verbose")
	}
}`

	afs, fset, err := strmap_to_ast(srcs)

	if err != nil {
		log.Fatal(err)
	}

	info, err := get_type_info(fset, afs)

	if err != nil {
		log.Fatalf("get_type_info failed:\n%s\n", err)
	}

	for id, obj := range info.Defs {
		fmt.Printf("%s: %q defines %v\n",
			fset.Position(id.Pos()), id.Name, obj)
	}
	println()
	for id, obj := range info.Uses {
		fmt.Printf("%s: %q uses %v\n",
			fset.Position(id.Pos()), id.Name, obj)
	}
}
```
