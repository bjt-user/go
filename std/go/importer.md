#### importer.Default()

```
$ go doc importer.Default
package importer // import "go/importer"

func Default() types.Importer
    Default returns an Importer for the compiler that built the running binary.
    If available, the result implements types.ImporterFrom.

    Default may be convenient for use in the simplest of cases, but most clients
    should instead use ForCompiler, which accepts a token.FileSet from the
    caller; without it, all position information derived from the Importer will
    be incorrect and misleading. See also the package documentation.
```

#### ForCompiler

Try this as an importer instead of `importer.Default()`:

```
second_importer := importer.ForCompiler(
    fset,
    "gc",
    func(path string) (io.ReadCloser, error) {
        file, err := os.Open(path)
        defer file.Close()

        return file, err
    },
)
```

=> this does not work, because it expects a `*.a` archive file.\
A compiled file that contains parts of the standard lib.\
And those files do not exist anywhere on my system.
