#### os.ExpandEnv

This will take the corresponding environment variable that is active \
on the current system and get the value of it:
```
proxy := os.ExpandEnv("$https_proxy")

fmt.Println(proxy)
```
