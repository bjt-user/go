https://pkg.go.dev/path/filepath

The `filepath` package is part of the `path` package and that is part of the standard lib.

#### FileInfo

It is helpful to know about the `FileInfo` interface that is also returned by `os.Stat()`.\
https://pkg.go.dev/os#FileInfo \
https://pkg.go.dev/io/fs#FileInfo

#### Walk

https://pkg.go.dev/path/filepath#Walk

Example of walk, that prints all files in dirs in the current dir recursively:
```
package main

import (
	"fmt"
	"io/fs"
	"path/filepath"
)

func main() {
	dir_to_walk := "."

	err := filepath.Walk(dir_to_walk, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}
		fmt.Printf("visited file or dir: %q", path)

		if info.IsDir() {
			fmt.Printf(" (%s is a dir.)\n", path)
		} else {
			fmt.Printf(" (%s is a file.)\n", path)
		}
		return nil
	})
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", dir_to_walk, err)
		return
	}
}
```

#### find a file

This function worked to find files that contain a string pattern:
```
func find_file(dir_path string, pattern string) []string {
	var matched_files []string
	err := filepath.Walk(dir_path, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(path, pattern) {
			fmt.Printf("path %v is %T\n", path, path)
			matched_files = append(matched_files, path)
		}

		return nil
	})
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", dir_path, err)
		return nil
	}

	return matched_files
}
```
