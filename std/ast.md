# ast

## basic info

#### about

https://pkg.go.dev/go/ast

> Package ast declares the types used to represent syntax trees for Go packages.

#### general usage

1. create a new FileSet
1. parse the files
1. inspect the files

## details

#### token.NewFileSet()

First you need to create a new FileSet:
```
fset := token.NewFileSet()
```

```
$ go doc -src token.FileSet
package token // import "go/token"

// A FileSet represents a set of source files.
// Methods of file sets are synchronized; multiple goroutines
// may invoke them concurrently.
//
// The byte offsets for each file in a file set are mapped into
// distinct (integer) intervals, one interval [base, base+size]
// per file. [FileSet.Base] represents the first byte in the file, and size
// is the corresponding file size. A [Pos] value is a value in such
// an interval. By determining the interval a [Pos] value belongs
// to, the file, its file base, and thus the byte offset (position)
// the [Pos] value is representing can be computed.
//
// When adding a new file, a file base must be provided. That can
// be any integer value that is past the end of any interval of any
// file already in the file set. For convenience, [FileSet.Base] provides
// such a value, which is simply the end of the Pos interval of the most
// recently added file, plus one. Unless there is a need to extend an
// interval later, using the [FileSet.Base] should be used as argument
// for [FileSet.AddFile].
//
// A [File] may be removed from a FileSet when it is no longer needed.
// This may reduce memory usage in a long-running application.
type FileSet struct {
        mutex sync.RWMutex         // protects the file set
        base  int                  // base offset for the next file
        files []*File              // list of files in the order added to the set
        last  atomic.Pointer[File] // cache of last file looked up
}
```

#### parser.ParseFile

```
func ParseFile(fset *token.FileSet, filename string, src any, mode Mode) (f *ast.File, err error)
ParseFile parses the source code of a single Go source file and returns
the corresponding ast.File node.
```

> The source code may be provided via the filename of the source file, or via the src parameter.

> Position information is recorded in the file set fset

Example Call after an empty FileSet `fset` has been created:
```
file, err := parser.ParseFile(fset, "src.go", nil, parser.ParseComments)
```
(The go file `src.go` must exist)

What mode to use?

```
type Mode

type Mode uint

A Mode value is a set of flags (or 0). They control the amount of source code parsed and other optional parser functionality.

const (
	PackageClauseOnly    Mode             = 1 << iota // stop parsing after package clause
	ImportsOnly                                       // stop parsing after import declarations
	ParseComments                                     // parse comments and add them to AST
	Trace                                             // print a trace of parsed productions
	DeclarationErrors                                 // report declaration errors
	SpuriousErrors                                    // same as AllErrors, for backward-compatibility
	SkipObjectResolution                              // skip deprecated identifier resolution; see ParseFile
	AllErrors            = SpuriousErrors             // report all errors (not just the first 10 on different lines)
)
```

#### ast.File

```
$ go doc -src ast.File
...
type File struct {
        Doc     *CommentGroup // associated documentation; or nil
        Package token.Pos     // position of "package" keyword
        Name    *Ident        // package name
        Decls   []Decl        // top-level declarations; or nil

        FileStart, FileEnd token.Pos       // start and end of entire file
        Scope              *Scope          // package scope (this file only). Deprecated: see Object
        Imports            []*ImportSpec   // imports in this file
        Unresolved         []*Ident        // unresolved identifiers in this file. Deprecated: see Object
        Comments           []*CommentGroup // list of all comments in the source file
        GoVersion          string          // minimum Go version required by //go:build or // +build directives
}
...
```

#### ast.Node (interface)

```
$ go doc -src ast.Node
package ast // import "go/ast"

// All node types implement the Node interface.
type Node interface {
	Pos() token.Pos // position of first character belonging to the node
	End() token.Pos // position of first character immediately after the node
}
```

`ast.File` implements this interface:
```
$ go doc -src ast.File | grep '^func'
...
func (f *File) End() token.Pos
func (f *File) Pos() token.Pos
```

`ast.FuncDecl` implements this interface:
```
$ go doc -src ast.FuncDecl | grep '^func'
func (d *FuncDecl) End() token.Pos
func (d *FuncDecl) Pos() token.Pos
```

`ast.CallExpr` implements this interface:
```
$ go doc -src ast.CallExpr | grep '^func'
func (x *CallExpr) End() token.Pos
func (x *CallExpr) Pos() token.Pos
```

#### End() and Pos() (functions)

The functions `End()` and `Pos()` return the first character of the node \
and the first character immediately after the node.\
(as documented by the interface `ast.Node`)

These two methods usually take a pointer receiver.

Example:
Given fn of type `*ast.FuncDecl` you can print the range of the function like this:
```
fmt.Printf("Main starts at character %v\n", fn.Pos())
fmt.Printf("and ends at character %v\n", fn.End())
```

## useful functions

#### ast.Fprint

seems to print an ast node:
```
ast.Fprint(os.Stdout, fset, fds[3], nil)
```
The third argument is the node.\
The last argument is a filter function.

to filter out nil fields:
```
ast.Fprint(os.Stdout, fset, fds[3], ast.NotNilFilter)
```

#### ast.Inspect (function)

```
$ go doc -src ast.Inspect
package ast // import "go/ast"

// Inspect traverses an AST in depth-first order: It starts by calling
// f(node); node must not be nil. If f returns true, Inspect invokes f
// recursively for each of the non-nil children of node, followed by a
// call of f(nil).
func Inspect(node Node, f func(Node) bool) {
	Walk(inspector(f), node)
}
```

You cannot effectively break out of this recursive function.\
Only in a very early stage by returning false early.\
This function resurses into the ast and there is no way of directly \
breaking out.

Returning false does not seem to make sense \
and oftentimes does not make a difference.

`return true` is basically similar to `continue` in a loop.

## examples

#### print all function names

```
$ go doc ast.FuncDecl
package ast // import "go/ast"

type FuncDecl struct {
        Doc  *CommentGroup // associated documentation; or nil
        Recv *FieldList    // receiver (methods); or nil (functions)
        Name *Ident        // function/method name
        Type *FuncType     // function signature: type and value parameters, results, and position of "func" keyword
        Body *BlockStmt    // function body; or nil for external (non-Go) function
}
    A FuncDecl node represents a function declaration.

func (d *FuncDecl) End() token.Pos
func (d *FuncDecl) Pos() token.Pos
```

```
package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

func main() {
	src := `
package main

var globvar int = 5

func lets_fail() {
	fmt.Fatalf("Foo...")
}

func print_help() {
	help_string := "HELP ME!"
	fmt.Println(help_string)
	lets_fail()
}

func main() {
    fmt.Println("Hello, World")
	print_help()
}

func main() {
	fmt.Println("a second main")
}`

	fset := token.NewFileSet()

	ast_file, _ := parser.ParseFile(fset, "", src, parser.AllErrors)

	for _, v := range ast_file.Decls {
		fn, ok := v.(*ast.FuncDecl)
		if !ok {
			continue
		}
		fmt.Printf("%v\n", fn)
	}
}
```

`ast.File.Decls` is a slice of type `Decl`.\
`ast.Decl` is an interface so you need to type assert it to `*ast.FuncDecl`.
