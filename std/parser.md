https://pkg.go.dev/go/parser

## parser.ParseExpr

This will probably be very useful for unit tests.

It only works for function calls and expressions like `1` or `1 + 1`.\
An assignment is not an expression.\
A function declaration or function body is not an expression.

An expression is:
- a function call like `my_func("foo")`
- a math expression like `3 + 4 + 5 - x`
- a string
- a bool like `false`

```
$ go doc -src parser.ParseExpr
package parser // import "go/parser"

// ParseExpr is a convenience function for obtaining the AST of an expression x.
// The position information recorded in the AST is undefined. The filename used
// in error messages is the empty string.
//
// If syntax errors were found, the result is a partial AST (with [ast.Bad]* nodes
// representing the fragments of erroneous source code). Multiple errors are
// returned via a scanner.ErrorList which is sorted by source position.
func ParseExpr(x string) (ast.Expr, error) {
	return ParseExprFrom(token.NewFileSet(), "", []byte(x), 0)
}
```

#### Example 1:
```
package main

import (
	"fmt"
	"go/parser"
	"log"
	"reflect"
)

func main() {
	my_str := `fmt.Println("hello world")`

	ast_expr, err := parser.ParseExpr(my_str)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Type:", reflect.TypeOf(ast_expr))
	fmt.Println("Value:", reflect.ValueOf(ast_expr))
}
```
outputs:
```
Type: *ast.CallExpr
Value: &{0xc000010030 12 [0xc000076060] 0 26}
```

#### Example 2:
```
my_str := `i`

	ast_expr, err := parser.ParseExpr(my_str)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Type:", reflect.TypeOf(ast_expr))
	fmt.Println("Value:", reflect.ValueOf(ast_expr))
```
outputs:
```
Type: *ast.Ident
Value: i
```

#### function ParseDir

```
func ParseDir(fset *token.FileSet, path string, filter func(fs.FileInfo) bool, mode Mode) (pkgs map[string]*ast.Package, first error)
```

This worked:
```
fset := token.NewFileSet()

pkg_map, err := parser.ParseDir(fset, "./cowsay",
    func(finfo fs.FileInfo) bool {
        if strings.HasSuffix(finfo.Name(), ".go") {
            return true
        }
        return false
    }, parser.SkipObjectResolution)

if err != nil {
    log.Fatal(err)
}

for key, value := range pkg_map {
    fmt.Printf("%v: %v\n", key, value)
}
```
outputs:
```
main: &{main scope 0x0 {}
 map[] map[cowsay/main.go:0xc0001080a0]}
```

Its a map that seems to contain a map of go files.\
And you can probably exclude `_test.go` files in the filter func.

What it doesn't do is parse the imported external packages of the module.\
It is just a way to parse an entire dir without using a loop with `ParseFile`.
