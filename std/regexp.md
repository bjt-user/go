https://pkg.go.dev/regexp

This package is part of the standard library.

#### example 1

Extended example from the first page of docs:
```
package main

import (
	"fmt"
	"regexp"
)

func main() {
	// Compile the expression once, usually at init time.
	// Use raw strings to avoid having to quote the backslashes.
	var validID = regexp.MustCompile(`^[a-z]+\[[0-9]+\]$`)

	my_string := "zed[333]"

	fmt.Println(validID.MatchString("adam[23]"))
	fmt.Println(validID.MatchString("eve[7]"))
	fmt.Println(validID.MatchString("Job[48]"))
	fmt.Println(validID.MatchString("snakey"))
	fmt.Println(validID.MatchString(my_string))
}
```

prints
```
true
true
false
false
true
```

#### regexp.Match() vs. regexp.MatchString()

https://pkg.go.dev/regexp#Match

https://pkg.go.dev/regexp#MatchString

`Match()` works with `[]byte` while `MatchString()` expects `string` as input.

#### regexp.MatchString()

https://pkg.go.dev/regexp#MatchString
```
func MatchString(pattern string, s string) (matched bool, err error)
```

example:
```
fmt.Println(regexp.MatchString("^foo", "fooBAR"))
```
will print
```
true <nil>
```
