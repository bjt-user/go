#### general

```
func Indent(dst *bytes.Buffer, src []byte, prefix, indent string) error
```

Can be used as a pretty print function.

#### example

Input file ("example.json"):
```
{"foo": 3,"bar": "hi"}
```

```
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
)

func main() {
	var dest bytes.Buffer

	file, err := os.Open("example.json")

	defer file.Close()

	bytes, err := io.ReadAll(file)

	if err != nil {
		panic(err)
	}

	err = json.Indent(&dest, bytes, "", "\t")

	if err != nil {
		panic(err)
	}

	fmt.Println(dest.String())
}
```
outputs:
```
{
        "foo": 3,
        "bar": "hi"
}
```
