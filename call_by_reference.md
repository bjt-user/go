#### change the value of a variable through a function

```
package main

import "fmt"

func change_string(some_string *string) {
	*some_string = "new string"
}

func main() {
	my_string := "oh hello there"

	fmt.Println(my_string)

	change_string(&my_string)

	fmt.Println(my_string)
}
```

#### maps and slices

https://go.dev/blog/maps

> Map types are reference types, like pointers or slices

When passing a map to a function you dont need to pass a pointer to change the map.

#### dereferencing pitfall (dereferencing string inside function)

When you dereference a string inside a function and want to slice that string \
you have to put braces around the string like this:
```
package main

import "fmt"

func remove_first(random *string) {
	*random = (*random)[1:]
}

func main() {
	my_string := "vim-go"

	remove_first(&my_string)

	fmt.Println(my_string)
}
```

This would NOT work:
```
func remove_first(random *string) {
	*random = *random[1:]
}
```
outputs:
```
invalid operation: cannot slice random (variable of type *string)
```

Probably because the `*` is applied after the index (or `[` symbols) is applied.
