https://go.dev/tour/methods/9

> An interface type is defined as a set of method signatures.

#### basic idea

You have two or more structs.\
And these structs have methods with the same name and same return values.\
These methods can have different implementations for every struct.

Now you can define an interface that groups the structs by these methods.

Then you can write functions with the **interface as argument**.\
(Interfaces can not be receivers.)\
(You cannot directly access the fields of the underlying struct from the interface,\
but you can define methods that return fields.)\
And in the functions that have the interface as argument you can use all the methods\
defined in the interface on the interface.

Since you cannot access fields directly from the interface it might make sense to use \
getter and setter methods as it is done in Java.\
But in `Go` people dont really seem to do this...

#### TODO: return interfaces

Return interface types in your interface functions to changes fields in your structs.\
You will probably need Getter and Setter methods like in java.

#### example

Note that in this example the implementation of the methods is the same for the two structs.\
One of the benefits of interfaces is that the implementation of the methods can be different for every struct.
```
package main

import (
	"fmt"
	"log"
	"strconv"
	"time"
)

type person struct {
	name       string
	birth_year int
}

type dog struct {
	name       string
	birth_year int
}

func (p person) walk() {
	fmt.Printf("Person %s walks on two legs.\n", p.name)
}

func (p person) get_birth_year() int {
	return p.birth_year
}

func (d dog) walk() {
	fmt.Printf("Dog %s walks on four legs.\n", d.name)
}

func (d dog) get_birth_year() int {
	return d.birth_year
}

type creature interface {
	get_birth_year() int
	walk()
}

func calculate_age(c creature) int {
	current_year, err := strconv.Atoi(time.Now().Format("2006"))
	if err != nil {
		log.Fatal(err)
		return -1
	}
	return current_year - c.get_birth_year()
}

func main() {
	frank := person{"Frank", 1999}
	goofy := dog{"Goofy", 2015}

	frank.walk()

	franks_age := calculate_age(frank)

	fmt.Println("Frank is", franks_age, "years old.")

	goofy.walk()

	goofy_age := calculate_age(goofy)

	fmt.Println("Goofy is", goofy_age, "years old.")
}
```

#### the empty interface

https://go.dev/tour/methods/14

```
package main

import "fmt"

func describe(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var i interface{}
	describe(i)

	i = 42
	describe(i)

	i = "hello"
	describe(i)
}
```
outputs
```
(<nil>, <nil>)
(42, int)
(hello, string)
```

## troubleshooting

#### sneaky interfaces

Sometimes data types might be shown as structs or pointers to structs by Go \
but you still have to use a `type assertion` to use it as a struct.
