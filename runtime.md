#### documentation

https://go.dev/doc/faq#runtime

> Does Go have a runtime?

> Go has an extensive runtime library, often just called the runtime, that is part of every Go program.\
This library implements garbage collection, concurrency, stack management, and other critical features of the Go language.\
Although it is more central to the language, Go’s runtime is analogous to libc, the C library.

> It is important to understand, however, that Go’s runtime does not include a virtual machine, such as is provided by the Java runtime.\
Go programs are compiled ahead of time to native machine code (or JavaScript or WebAssembly, for some variant implementations).\
Thus, although the term is often used to describe the virtual environment in which a program runs, in Go the word “runtime” is just the name given to the library providing critical language services.

#### general

A go binary seems to have a Go runtime baked in.

In that runtime seem to be variables like `GOROOT` and `GOPATH`.

I could not find any original documentation on how the Go build process \
and the Go runtime work.

#### goroot

```
$ strings mdtoc | grep -i "goroot"
...
runtime.GOROOT
...
runtime.defaultGOROOT
runtime.defaultGOROOT.str
```

You can get `GOROOT` like this:
```
fmt.Printf("GOROOT: %s\n", runtime.GOROOT())
```
But this will not give a different GOROOT if you have set one with `go env GOROOT=/foo` \
once the binary is compiled.

When you use `-trimpath` the `runtime.GOROOT()` function will output nothing:
```
$ go build -trimpath main.go
$ ./main
GOROOT:
$ go build main.go
$ ./main
GOROOT: /usr/lib/go
```
