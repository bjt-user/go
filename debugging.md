## gdb

https://go.dev/doc/gdb

```
go build main.go
```

```
gdb main
```

You when trying to set breakpoints at functions you need to use the package name \
in front.
```
(gdb) l 100
95	func main() {
96		file_name := os.Args[1]
97
98		var head *heading = nil
99
100		parse_file(file_name, &head)
101
102		//print_list(head)
103
104		print_children_recursive(head, "")
(gdb) break main
Function "main" not defined.
```

```
break main.main
```

next problem -> I go into assembly code just by pressing `n`...

## delve

https://github.com/go-delve/delve

```
sudo pacman -S delve
```

```
sudo apt install delve
```

But your go version and delve version need to fit together somehow:
```
$ dlv debug *.go
Version of Delve is too old for Go version 1.22.2 (maximum supported version 1.21, suppress this error with --check-go-version=false)
```

```
$ git clone https://github.com/go-delve/delve
$ cd delve
$ go install github.com/go-delve/delve/cmd/dlv
```
This installs the binary to
```
${HOME}/go/bin/dlv
```

```
${HOME}/go/bin/dlv debug *.go
```

#### debugging your module

```
dlv debug .
```

```
break main.main
```

```
n
```

#### invoking delve

This kinda worked when I did not have a main function with a main file:
```
dlv test *.go
```

```
break Test_filetree
```

```
n
```

```
list
```

#### passing cli args

You just use `--` to pass cli args to your program:
```
${HOME}/go/bin/dlv debug *.go -- tests/test.md
```

#### settings breakpoints

Using file name and line number worked once, but you need a line with a statement:
```
(dlv) break tree_test.go:5
Breakpoint 1 set at 0x604e56 for command-line-arguments.Test_filetree() ./tree_test.go:5
```

To break at functions:
```
break [function_name]
```

#### print variables

Show your local vars:
```
locals
```

print variables:
```
p myvar
```

(But strings are not printed in full length.)
```
config max-string-len 1000
```

After that your string will be printed in full if its length is below 1000.\
(This will only last for the current delve session.)

#### display

```
help display
```

Add an expression or variable to the list:
```
display -a myvar
```

#### print strings.Builders as strings

```
print string(my_builder.buf)
```

#### TODO: Core Dumps

https://go.dev/wiki/CoreDumpDebugging

## printf debugging

#### print current line and file name

This will also print a program counter and a bool if the function was successful:
```
fmt.Println(runtime.Caller(0))
```
