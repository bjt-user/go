https://pkg.go.dev/unicode

#### unicode.In()

To use this function you first need to create a custom RangeTable.

```
// Define a custom RangeTable
my_range_table := &unicode.RangeTable{
    R16: []unicode.Range16{
        {Lo: 'A', Hi: 'Z', Stride: 1}, // 'A' to 'Z'
        {Lo: 'a', Hi: 'z', Stride: 1}, // 'a' to 'z'
    },
}

fmt.Println(unicode.In('p', my_range_table))
fmt.Println(unicode.In('3', my_range_table))
```
outputs:
```
true
false
```

Use cases for this function are probably very rare.

The function `strings.ContainsAny` will usually be better suited:
```
fmt.Println(strings.ContainsAny("yu\\mmy", "\\z#"))
```
outputs:
```
true
```
