https://go.dev/src/cmd/compile/internal/ssa/README

#### use of internal package not allowed

When trying to import:
```
import (
        "cmd/compile/internal/ssa"
)
```
it will not compile:
```
package command-line-arguments
        main.go:4:2: use of internal package cmd/compile/internal/ssa not allowed

shell returned 1
```
