Inside an existing go project this command
```
go doc
```

or
```
go doc .
```
provided a short (probably auto-generated) summary of all functions, types,\
and vars of the package.

#### documentation on functions from the standard library

```
go doc builtin.make
```

```
go doc fmt.Printf
```

#### view source code

```
$ go doc -src token.NewFileSet
package token // import "go/token"

// NewFileSet creates a new file set.
func NewFileSet() *FileSet {
        return &FileSet{
                base: 1, // 0 == NoPos
        }
}
```