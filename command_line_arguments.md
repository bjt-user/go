POSIX cli arguments:\
https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html

## os.Args

```
if len(os.Args) > 1 && os.Args[1] == "." {
    fmt.Println("You passed at least 1 argument and the first argument was a dot.")
}
```

## flag

`flag` is part of the standard library:\
https://pkg.go.dev/flag@go1.23.1

#### example (int flag)

```
package main

import (
	"flag"
	"fmt"
)

func main() {
	var n_flag = flag.Int("n", 1234, "help message for flag n")

	flag.Parse()
	fmt.Println("n is", n_flag)
	fmt.Println("n is", *n_flag)
	fmt.Printf("%T\n", n_flag)
}
```

```
$ go run main.go -n 34
n is 0xc000012100
n is 34
*int
```

`1234` is the default value:
```
$ go run main.go
n is 0xc000012100
n is 1234
*int
```

#### example string flag

```
func main() {
        var file_flag = flag.String("f", "", "help message for flag f")

        flag.Parse()
        fmt.Println("f is", *file_flag)
}
```

```
$ go run string_arg.go -f myfile.txt
f is myfile.txt
```

#### example short and long options

```
package main

import (
	"flag"
	"fmt"
)

func main() {
	// Define a variable to store the flag value
	var name string

	// Define both the short and long flags
	flag.StringVar(&name, "n", "", "Your name (short version)")
	flag.StringVar(&name, "name", "", "Your name (long version)")

	flag.Parse()

	fmt.Printf("Hello, %s!\n", name)
}
```

```
$ go run short_and_long.go -n Mark
Hello, Mark!
$ go run short_and_long.go --name Mark
Hello, Mark!
```

#### print usage

This will print the usage if no cli args were given:
```
if flag.NFlag() == 0 {
    flag.Usage()
	os.Exit(1)
}
```

#### flag.Visit()

This will print all flags that have been set:
```
flag.Visit(func(f *flag.Flag) {
    fmt.Println(f.Name)
})
```

That way you can check if a flag was provided:\
https://stackoverflow.com/questions/35809252/check-if-flag-was-provided-in-go

This worked:
```
func is_flag_passed(flag_name string) bool {
        found := false
        flag.Visit(func(f *flag.Flag) {
                if flag_name == f.Name {
                        found = true
                }
        })

        return found
}
```

#### TODO: prevent the use of exclusive flags

Especially if you have string flags that should not be used in conjunctions \
this seems difficult.\
And with the `flag` package you have short flags and long flags that do the same thing.

You should be able to build a function that takes a string slice.\
Then iterates over the slice and if all flags are set it should return true \
or directly log.Fatal("foo").

#### general cli arg strategy

The examples from the `flag` package from the standard lib uses "package level" variables.\
(are these considered global variables?)

It would be good if your cli args are global variables \
otherwise you would need to grind them through almost every function \
of your program.

A struct does not really make sense since you only need 1 instance of the \
cli arguments.

So global vars or a map may be the way to go.\
(a map is not really useful, since you will have different data types for your flags)

Maybe using the `init()` function is helpful as well.

The downside of global vars is that functions and methods are not as encapsulated.\
Unit tests will only work when using your entire project/program.\
You should try to avoid global variables.

Another way would be to use a struct and pass that struct to a lot of functions.\
Or only pass fields of that struct.

#### example with global (or package level) vars

This worked and I could access the flag variables from any file in my package.

This is the file where the cli arg parsing was done.\
`cli_args.go`:
```
package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

var (
	flag_file   string
	flag_search bool
	flag_all    bool
)

func init() {
	flag.StringVar(&flag_file, "f", "", "print relevant targets of the given Makefile")
	flag.StringVar(&flag_file, "file", "", "print relevant targets of the given Makefile")
	flag.BoolVar(&flag_search, "s", false, "search current dir for Makefiles")
	flag.BoolVar(&flag_search, "search", false, "search current dir for Makefiles")
	flag.BoolVar(&flag_all, "a", false, "print all targets")
	flag.BoolVar(&flag_all, "all", false, "print all targets")
}

func evaluate() error {
	var err error

	if (flag_search) || (flag.NFlag() == 0) {
		search_result()
	}
	if flag_file != "" {
		fmt.Println("Lets call the function that will parse the Makefile", flag_file)
		single_file_result(flag_file)
		os.Exit(0)
	} else {
		err = errors.New("File path is an empty string.")
		return err
	}
	return err
}
```

In order for `go test` to work, you cannot have `flag.Parse()` in any `init()` function.\
So I called it in `main()`, after the variables where defined in the `init()` function.

`main.go`:
```
package main

import (
	"flag"
	"log"
)

func main() {
	flag.Parse()

	err := evaluate()

	if err != nil {
		log.Fatal(err)
	}
}
```

After that I could access the variables `flag_file`, `flag_search`, `flag_all` in every file \
of my package.

#### example without globals and with struct

Use a struct and a function that returns an object of that struct.\
Like a constructor.\
Then you need a function that evaluations those cli args and calls sections of the \
program to act according to these flags.

`cli_args.go`
```
package main

import (
	"errors"
	"flag"
	"os"
)

type cli_args struct {
	file_path   string
	search_flag bool
	all_flag    bool
}

func parse_cli_args() cli_args {
	args := cli_args{}

	flag.StringVar(&args.file_path, "f", "", "print relevant targets of the given Makefile")
	flag.StringVar(&args.file_path, "file", "", "print relevant targets of the given Makefile")
	flag.BoolVar(&args.search_flag, "s", false, "search current dir for Makefiles")
	flag.BoolVar(&args.search_flag, "search", false, "search current dir for Makefiles")
	flag.BoolVar(&args.all_flag, "a", false, "print all targets")
	flag.BoolVar(&args.all_flag, "all", false, "print all targets")
	flag.Parse()

	return args
}

func (args cli_args) evaluate() error {
	var err error

	if (args.search_flag) || (flag.NFlag() == 0) {
		search_result()
	}
	if args.file_path != "" {
		single_file_result(args)
		os.Exit(0)
	} else {
		err = errors.New("File path is an empty string.")
		return err
	}
	return err
}
```

Then use the `parse_cli_args()` function to instantiate a cli_args object in main.

Then you call the `evaluate()` function and check for errors and you are done in main.

`main.go`
```
package main

import (
	"log"
)

func main() {
	program_flags := parse_cli_args()

	err := program_flags.evaluate()

	if err != nil {
		log.Fatal(err)
	}
}
```
