#### read file line by line

This is the stackoverflow recommended way to read a file line by line:
```
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.Open("myfile.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// optionally, resize scanner's capacity for lines over 64K
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
```

#### read part of a file (os.ReadAt)

```
func (f *File) ReadAt(b []byte, off int64) (n int, err error)
    ReadAt reads len(b) bytes from the File starting at byte offset off.
    It returns the number of bytes read and the error, if any. ReadAt always
    returns a non-nil error when n < len(b). At end of file, that error is
    io.EOF.
```

Start reading at the 11th character (start counting at 0).\
Stop reading after the 10th character/byte.
```
package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.Open("foo.txt")

	if err != nil {
		log.Fatal(err)
	}

	my_buf := make([]byte, 10)

	file.ReadAt(my_buf, 11)

	fmt.Println(string(my_buf))
}
```
