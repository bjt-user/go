#### List information about a package

If you are in the dir of a go package with a `go.mod` file:
```
go list -json
```

or to get info about the `strings` package from std:
```
go list -json strings
```
