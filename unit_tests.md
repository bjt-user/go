#### documentation

https://pkg.go.dev/testing

https://pkg.go.dev/cmd/go#hdr-Testing_flags

https://en.wikipedia.org/wiki/Unit_testing

In computer programming, unit testing, a.k.a. component or module testing, is a form of software testing by which isolated source code is tested to validate expected behavior.

https://go.dev/doc/tutorial/add-a-test

```
go help test
```

#### module required

It looks like you need a module for unit tests:
```
$ go test
go: go.mod file not found in current directory or any parent directory; see 'go help modules'
```

#### how to write a unit test

1. create a module and a .go file with a func you want to test
1. create a file that ends with `_test.go`
1. both files need to be in the same package
1. both files need to be in the same directory (there has to be another way...)
1. if the function you want to test looks like this `func Change_string(change_me *string)` \
you need to call the test function like this:
```
func TestChange_string(t *testing.T) {
```
1. type `go test`


#### working example

```
$ cat main.go
package main

import (
	"fmt"
	"strings"
)

func Change_string(change_me *string) {
	*change_me = strings.ReplaceAll(*change_me, "foo", "shoe")
}

func main() {
	var my_string string = "foobar"

	Change_string(&my_string)

	fmt.Println(my_string)
}
```

```
$ cat foo_test.go
package main

import (
	"fmt"
	"log"
	"testing"
)

func TestChange_string(t *testing.T) {
	my_string := "okokfoo"

	Change_string(&my_string)

	fmt.Println("string after call to Change_string:", my_string)

	if my_string == "" {
		log.Fatal("The string shouldnt be empty after function call")
	}
}
```

```
$ cat go.mod
module unit-test-tries

go 1.22.2
```

#### Check and fail

You can use `Fatalf` to fail and print a message when the test failed:
```
func Test_inspect_line(t *testing.T) {
...
spacestart_lp := inspect_line(" .RECIPEPREFIX = t       ")
if spacestart_lp.starts_with_tab == true {
        t.Fatalf("spacestart_lp does not start with a tab")
}
```

With just
```
t.Fail()
```
it is not possible to send a message and you will not be able to see where in the function \
the test has failed.

#### code coverage

https://en.wikipedia.org/wiki/Code_coverage

> In software engineering, code coverage, also called test coverage, is a percentage measure of the degree to which the source code of a program is executed when a particular test suite is run.\
A program with high code coverage has more of its source code executed during testing, which suggests it has a lower chance of containing undetected software bugs compared to a program with low code coverage.\
Many different metrics can be used to calculate test coverage.\
Some of the most basic are the percentage of program subroutines and the percentage of program statements called during execution of the test suite.

```
go test --cover
```

#### T functions

https://pkg.go.dev/testing#T

There are different ways to fail tests.\
You can probably continue running all tests even though a test has failed.


#### testing functions that write to stdout

For smaller functions you might want to return a string instead of writing to standard out.

Another way is to use `fmt.Fprintf` with an `io.Writer` that is `os.Stdout` in production code,\
but `bytes.Buffer` in tests.


example:
```
func logger(writer io.Writer) {
	fmt.Fprintf(writer, "hello world\n")
}

func main() {
	// run the function in production
	logger(os.Stdout)

	var mybuf bytes.Buffer

	// run the function in testing
	logger(&mybuf)

	fmt.Printf(mybuf.String())
}
```

#### test only one function

From https://pkg.go.dev/cmd/go#hdr-Testing_flags:\
> run regexp\
Run only those tests, examples, and fuzz tests matching the regular expression.

```
go test -v -run "^Test_get_calls$"
```
