## time and date

#### go time and date codes

| go code | meaning |
| --- | --- |
| `2006` | YYYY |
| `01` | MM |
| `02` | DD |

#### oneliner to get the current date

```
package main

import (
	"time"
)

func main() {
	// go has a very weird date layout:
	println(time.Now().Format("2006-01-02"))
}
```

#### more elegant oneliner (LATEST GO VERSION ONLY)

```
package main

import "time"

func main() {
	println(time.Now().Format(time.DateOnly))
}
```

#### print constants from the go package

```
package main

import "time"

func main() {
	println(time.RFC822)
}
```

The latest version of go has this neat constant:
```
println(time.DateOnly)
```
which prints
```
2006-01-02
```

And that enables you to get the date without having to use the weird code:
```
println(time.Now().Format(time.DateOnly))
```

#### sleep

Sleep 5 seconds:
```
time.Sleep(5 * time.Second)
```
