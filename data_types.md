#### print data type of a variable

This will print the value and the data type of the variable `count`:
```
fmt.Printf("variable count=%v is of type %T \n", count, count)
```

For a returned error of an os.ReadFile method it looked like this:
```
variable err="open file.txt: no such file or directory" is of type *fs.PathError
```

When the file exists, it looks like this:
```
variable err="<nil>" is of type <nil>
```
(no error is returned)

The error is a pointer and when there is no error it points to NULL.

## type conversions

#### string to int

```
var year_str string = "2010"

year_int, err := strconv.Atoi(year_str)

if err != nil {
    log.Fatal(err)
}

fmt.Printf("year_int (%T): %v\n", year_int, year_int)
```
