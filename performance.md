## profiling

From "The Go Programming Language" (Alan A. A. Donovan, Brian W. Kernighan) (p. 324)

> When we wish to look carefully at the speed of our programs, the best \
technique for identifying the critical code is "profiling".

> Profiling is an automated approach to performance measurement based on \
sampling a number of profile events during execution, then extrapolating \
from them during a post-processing step; the resulting statistical summary \
is called a profile.

> A CPU profile identifies the functions whose execution requires the most CPU time.

> A heap profile identifies the statements responsible for allocating the most memory.

> A blocking profile identifies the operations responsible for blocking goroutines \
the longest, such as system calls, channel sends and receives, and acquisition \
of locks.

```
go test -cpuprofile=cpu.out
```
```
go test -blockprofile=block.out
```
```
go test -memprofile=mem.out
```

Then you have to analyze those created files using the `pprof` tool.

#### example

This might help identifying slow functions.
```
go test -cpuprofile=cpu.out
```

```
go tool pprof -text -nodecount=20 cpu.out
```

The list is sorted so that \
the functions that consume the most CPU cycles appear first.

## benchmarks

https://pkg.go.dev/testing#hdr-Benchmarks

A benchmark function is placed inside `*_test.go` files and looks like this:
```
func Benchmark_new_func_center(b *testing.B) {
	afps, fset, _ := parse_dir_afps("test_files/makefile_parser")

	for i := 0; i < b.N; i++ {
		new_func_center(fset, afps)
	}
}
```
The `b.N` value is used to run your function multiple times to measure its speed.

Benchmarks are NOT run by default when using `go test`.\
You have to specify the `-bench` option.

```
go test -bench="."
```

This means your function was run 100 times and it took on average \
12863424 ns per call/loop.
```
Benchmark_new_func_center-8   	     100	  12863424 ns/op
```

#### using time pkg to measure duration of a function

Benchmarking might not be accurate when things are initialized and \
loaded into RAM.\
The second time a function is called might be many times faster.

A simpler way to measure the duration of a function is using the `time` pkg:
```
func foo() {
    func_start := time.Now()

    // function body

    func_end := time.Now()

    fmt.Printf("Function foo() took %v.\n", func_end.Sub(func_start))
}
```

## performance improvements

#### convert slices to linked lists

Especially for slices to structs it could improve performance to \
convert those to linked lists.

Either single linked lists or doubly linked lists for a simpler handling.

#### multithreading

Especially for initializing data at the beginning of the program \
or when reading potentially large files.
