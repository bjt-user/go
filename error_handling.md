https://go.dev/blog/error-handling-and-go

```
f, err := os.Open("filename.ext")
if err != nil {
    log.Fatal(err)
}
```

#### the error type

`error` is a builtin type in Go.

You can use the `errors.New()` function to construct an error:
```
err := errors.New("String is empty, no file given")
```

```
$ go doc builtin.error
package builtin // import "builtin"

type error interface {
	Error() string
}
    The error built-in interface type is the conventional interface for
    representing an error condition, with the nil value representing no error.
```

#### example on how to implement a function that returns an error

```
package main

import (
	"errors"
	"fmt"
	"log"
)

func read_file(file_path string) (string, error) {
	var err error

	if file_path == "" {
		err = errors.New("String is empty, no file given")
		return "", err
	}
	return "dummy content", err
}

func main() {
	my_content, err := read_file("")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(my_content)
}
```
