## manual implementation

#### simple example

This is a simple example that pushes nodes to the front of a linked \
list and prints the entire list:
```
package main

import "fmt"

type child struct {
	name string
	age  int
	next *child
}

// push new node to the front of the list
func push(my_head **child, my_name string, my_age int) {
	var new_node *child = &child{my_name, my_age, *my_head}
	*my_head = new_node
}

func print_list(my_head *child) {
	for my_head != nil {
		fmt.Printf("%s, ", my_head.name)
		fmt.Printf("%d\n", my_head.age)
		my_head = my_head.next
	}
}

func main() {
	// initialize linked list
	var head *child = nil

	push(&head, "Adam", 12)
	push(&head, "Eva", 10)

	print_list(head)
}
```

#### push to front of the list

```
func push(my_head **child, my_name string, my_age int) {
	var new_node *child = &child{my_name, my_age, *my_head}
	*my_head = new_node
}
```

#### print list

```
func print_list(my_head *child) {
	for my_head != nil {
		fmt.Printf("%s, ", my_head.name)
		fmt.Printf("%d\n", my_head.age)
		my_head = my_head.next
	}
}
```

#### example where nodes are appended

```
package main

import "fmt"

type child struct {
	name string
	age  int
	next *child
}

// push new node to the front of the list
func push(my_head **child, my_name string, my_age int) {
	var new_node *child = &child{my_name, my_age, *my_head}
	*my_head = new_node
}

func print_list(my_head *child) {
	for my_head != nil {
		fmt.Printf("%s, ", my_head.name)
		fmt.Printf("%d\n", my_head.age)
		my_head = my_head.next
	}
}

func append(my_head **child, my_name string, my_age int) {
	if *my_head == nil {
		println("cant append to an empty list, so it will be pushed")
		push(my_head, my_name, my_age)
	} else {
		var new_node *child = &child{my_name, my_age, nil}
		var cur_node *child = *my_head
		for cur_node != nil {
			if cur_node.next == nil {
				cur_node.next = new_node
				return
			}

			cur_node = cur_node.next
		}
	}
}

func main() {
	// initialize linked list
	var head *child = nil

	append(&head, "Holger", 44)
	append(&head, "Adam", 12)
	append(&head, "Eva", 10)
	append(&head, "Horst", 14)

	print_list(head)
}
```

#### append to a list

```
func append(my_head **child, my_name string, my_age int) {
	if *my_head == nil {
		println("cant append to an empty list, so it will be pushed")
		push(my_head, my_name, my_age)
	} else {
		var new_node *child = &child{my_name, my_age, nil}
		var cur_node *child = *my_head
		for cur_node != nil {
			if cur_node.next == nil {
				cur_node.next = new_node
				return
			}

			cur_node = cur_node.next
		}
	}
}
```

## standard library

There is a `list` package, but it does not seem to use structs \
and you are only able to store one value per list element.

https://pkg.go.dev/container/list@go1.22.2

Maybe it makes sense to store pointers to a struct element in it?

#### using list package in combination with a struct

Using the list package with structs does not really work, because you cannot \
access a struct member from the `Value` member of the `Element` struct.

(https://cs.opensource.google/go/go/+/master:src/container/list/list.go)

So this will **NOT** work:
```
package main

import (
	"container/list"
	"fmt"
)

type child struct {
	name string
	age  int
}

func main() {
	child_list := list.New()

	child_list.PushBack(child{"Adam", 12})

	child_list.PushBack(child{"Eva", 10})

	child_list.PushBack(child{"Juergen", 55})

	for element := child_list.Front(); element != nil; element = element.Next() {
		fmt.Printf("%v\n", element.Value)
	}

	fmt.Printf("The first child is %v\n", child_list.Front().Value.name)
}
```

Because the value is type `any` and not of custom type `child`.
