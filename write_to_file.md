#### example with a Writer

This will open a file for writing (and reading) or create it if it doesnt exist.
```
func main() {
	myvar := "foo"
	myfile, err := os.OpenFile("foo.txt", os.O_RDWR|os.O_CREATE, 0644)

	if err != nil {
		log.Fatal(err)
	}

	file_writer := bufio.NewWriter(myfile)

	fmt.Fprintf(file_writer, "%s\n", myvar)

	file_writer.Flush() // Don't forget to flush!

	myfile.Close()
}
```
