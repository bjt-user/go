Composite types are created by combining **basic types**.

Composite types are:
- arrays
- slices
- maps
- structs
