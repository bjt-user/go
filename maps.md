https://go.dev/blog/maps

#### example program

```
package main

import "fmt"

func main() {
	my_inventory := map[string]int{
		"banana":      5,
		"screwdriver": 0,
		"screws":      0,
	}

	my_inventory["screwdriver"] = 2

	fmt.Printf("%v\n", my_inventory)
}
```
will output
```
map[banana:5 screwdriver:2 screws:0]
```

print single values like this:
```
fmt.Printf("%v\n", my_inventory["banana"])
```

You can add a new key-value-pair just with this line:
```
my_inventory["cable"] = 10
```

#### initializing maps

use `make` to initialize an empty map
```
level_count := make(map[int]int)
```

#### utilizing zero values

The default value of a key is the "zero" value.\
The "zero" value of an `int` is 0.

This example will automatically create a key when it doesn't exist \
and increment from 0:
```
func count_levels(headings []heading) map[int]int {
	level_count := make(map[int]int)

	for index, _ := range headings {
		switch headings[index].level {
		case 1:
			level_count[1]++
		case 2:
			level_count[2]++
		case 3:
			level_count[3]++
		case 4:
			level_count[4]++
		case 5:
			level_count[5]++
		case 6:
			level_count[6]++
		}
	}

	return level_count
}
```
When a case condition is never met, the key-value pair will never be created.

#### append to a map

You can add a new key-value-pair to an existing map just with this line:
```
my_map["cable"] = 10
```

#### length of a map

```
len(my_map)
```

#### loop through maps

One way to loop through maps:
```
for key, value := range my_inventory {
    fmt.Printf("%v: %v\n", key, value)
}
```
But the iteration order is not specified.\
See https://go.dev/blog/maps ("iteration order").

> If you require a stable iteration order you must maintain a \
separate data structure that specifies that order.
