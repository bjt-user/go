#### convert integer to string

The `strconv` pkg can do it.

```
var bar string = strconv.Itoa(28)
```
