https://go.dev/doc/tutorial/compile-install

With go you do not need long and complicated Makefiles anymore because the `go` \
binary takes care of a lot of tasks.

```
sudo pacman -S go
```

#### building

```
go build main.go
```

If you have multiple files:
```
go build .
```
(might only work if you have created a module with `go mod init my-module`)

#### running

```
go run .
```

#### installing

See `go help install`.
```
go install
```

> Executables are installed in the directory named by the GOBIN environment \
variable, which defaults to $GOPATH/bin or $HOME/go/bin if the GOPATH \
environment variable is not set.\
Executables in $GOROOT are installed in $GOROOT/bin or $GOTOOLDIR instead of $GOBIN.

#### clean up executable

```
go clean
```
