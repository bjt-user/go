From "The Go Programming Language" (Alan A. A. Donovan, Brian W. Kernighan)

> "Import paths" are the strings that appear in import declarations:
```
import (
    "fmt"
    "github.com/go-sql-driver/mysql"
}
```

> To avoid conflicts, the import paths of all packages other than those \
from the standard library should start with the Internet domain name of the organization \
that owns or hosts the package; this also makes it possible to find packages.

## graph

![image info](./graphs/imports.svg)

## golang.org imports

How is this import resolved:
```
import "golang.org/x/telemetry"
```

```
$ curl https://golang.org/x/telemetry
<!DOCTYPE html>
<html lang="en">
<title>The Go Programming Language</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="go-import" content="golang.org/x/telemetry git https://go.googlesource.com/telemetry">
<meta http-equiv="refresh" content="0; url=https://pkg.go.dev/golang.org/x/telemetry">
</head>
<body>
<a href="https://pkg.go.dev/golang.org/x/telemetry">Redirecting to documentation...</a>
</body>
</html>
```

The meta information shows this git repo:
```
git clone https://go.googlesource.com/telemetry
```

What a convoluted import system...

## module: download dependencies to temp dir

When inside a module (go.mod file exists) \
you could do this to have your dependencies in the module dir:
```
GOMODCACHE="$(pwd)/.cache" go get
```
