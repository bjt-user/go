#### toggling

You can toggle booleans by using the not operator like a light switch \
without knowing its current value.

This will output `false`:
```
my_bool := true

my_bool = !my_bool

fmt.Printf("%v\n", my_bool, my_bool)
```
