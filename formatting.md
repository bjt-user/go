`gofmt` is the official formatting tool and is highly recommended by the Go team.

https://go.dev/blog/gofmt

There is a builtin formatting tool:
```
go fmt main.go
```

`vim-go` uses it on save (that is also in the go blog post).
