https://go.dev/blog/cgo

Above your `import "C"` statement you need to specify the C includes in comments!\
Like this:
```
/*
#include <stdlib.h>
#include <pulse/simple.h>
*/
import "C"
```
