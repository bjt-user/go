#### documentation

https://go.dev/ref/mod#introduction

#### modules

https://go.dev/ref/mod#introduction
> Modules are how Go manages dependencies.

#### history

The source code of external packages used to reside in \
`$GOPATH/src`.\
This changed when `go modules` were introduced.

For backward compatibility the `Check` method in `go/types` \
still searches there for external packages with the default importer.

#### including third party packages in your program

```
go mod init my-project-name
```
This will create a `go.mod` file.

As an example I will use a cowsay repo.\
I create this file as `main.go`:
```
package main

import (
	"fmt"

	cowsay "github.com/Code-Hex/Neo-cowsay/v2"
)

func main() {
	say, err := cowsay.Say(
		"Hello",
		cowsay.Type("default"),
		cowsay.BallonWidth(40),
	)
	if err != nil {
		panic(err)
	}
	fmt.Println(say)
}
```

When trying to run `main.go` it will tell me to execute this command:
```
go get github.com/Code-Hex/Neo-cowsay/v2
```

This will update the `go.mod` file\
and create a `go.sum` file with checksums.

After that
```
go run main.go
```
should work.

**Warning: It looks like the package name at the top of your code file needs to be called "main"!\
But you can name the project at the go mod init command whatever you want!**

#### remove unused packages

```
go mod tidy
```

#### go get

> `go get` is no longer supported outside a module.
        To build and install a command, use `go install` with a version,
        like `go install example.com/cmd@latest`
        For more information, see https://golang.org/doc/go-get-install-deprecation
        or run `go help get` or `go help install`.

#### go install

Don't specify the protocol (`https://`).\
You need to specify a version. (i.e. `@latest`)

```
$ go install github.com/Code-Hex/Neo-cowsay@latest
go: downloading github.com/Code-Hex/Neo-cowsay v1.0.4
go: downloading github.com/Code-Hex/go-wordwrap v1.0.0
go: downloading github.com/mattn/go-runewidth v0.0.7
go: downloading github.com/pkg/errors v0.8.1
package github.com/Code-Hex/Neo-cowsay is not a main package
```

Will install the package here:
```
:~/go $ find . -iname "*cowsay*"
./pkg/mod/github.com/!code-!hex/!neo-cowsay@v1.0.4
./pkg/mod/github.com/!code-!hex/!neo-cowsay@v1.0.4/cowsay.go
./pkg/mod/github.com/!code-!hex/!neo-cowsay@v1.0.4/cmd/cowsay
./pkg/mod/cache/download/sumdb/sum.golang.org/lookup/github.com/!code-!hex/!neo-cowsay@v1.0.4
./pkg/mod/cache/download/github.com/!code-!hex/!neo-cowsay
```

But it seems like I can only use external packages when I am inside a module:
```
$ go run main.go
main.go:4:2: package cowsay is not in std (/usr/lib/go/src/cowsay)
```

#### Exclamation marks in $GOPATH/pkg/mod

From the `go.mod` file:
```
require github.com/Code-Hex/Neo-cowsay/v2 v2.0.4
```

It seems like every upper case letter has an exclamation mark infront of it:\
`~/go/pkg/mod/github.com/!code-!hex/!neo-cowsay`

> Capital letters in module paths and versions are escaped using exclamation points \
(Azure is escaped as !azure) to avoid conflicts on case-insensitive file systems.

#### build cache

Go package seem to be cached also.

When you do `go clean` it will remove the cache and when you `go build` \
again it will download everything again.

```
packagefile cowsay=/home/user/.cache/go-build
```

#### module cache

https://go.dev/ref/mod#module-cache

The module cache is the directory where the go command stores downloaded module files.\
The module cache is distinct from the build cache, which contains compiled packages and other build artifacts.

The default location of the module cache is $GOPATH/pkg/mod. To use a different location, set the GOMODCACHE environment variable.
