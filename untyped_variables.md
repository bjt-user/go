https://en.wikipedia.org/wiki/Go_(programming_language)#Syntax

> A combined declaration/initialization operator was introduced \
that allows the programmer to write i := 3 or s := "Hello, world!", without specifying the types of variables used.\
This contrasts with C's int i = 3; and const char *s = "Hello, world!";. 

#### example program

```
package main


func main() {
	myvar := "hello";

	println(myvar);
}
```

This will output.
```
hello
```

And you don't have to specify the data type of the variable `myvar`.
