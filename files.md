#### basename

Print the basename of a file:
```
myfile := "myfiles/file.txt"

fmt.Println(path.Base(myfile))
```
