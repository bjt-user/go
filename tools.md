## x tools

#### unused functions (deadcode)

Warning: Google License (https://github.com/golang/tools/blob/master/LICENSE)

https://github.com/golang/tools/tree/master/cmd/deadcode

```
go install golang.org/x/tools/cmd/deadcode@latest
```

=> seems to work

Took around 1 second to execute.

In the directory with Go source files that have a `main` function:
```
deadcode .
```

#### uninstall an x tool

```
go clean -i golang.org/x/tools/cmd/callgraph
```

#### ssadump

```
go install -v golang.org/x/tools/cmd/ssadump@latest
```

```
ssadump -build F simple_example.go
```

This produces a "single static assignment" form.

Adding `D` will print comments that tell which var is \
of which data type at what line:
```
ssadump -build DFI simple_example.go
```

An `init()` func is always shown even though you dont use one.\
`I` will at least make that init func shorter in output.

## third party tools

#### additional resources

overview over the standard lib, shows which functions implement \
which interface:\
https://docs.go101.org/std/index.html

#### go-callvis

https://github.com/ondrajz/go-callvis/tree/master

```
make build
```

It will take a couple of seconds, and then open up a browser with the calltree:
```
go-callvis .
```

Without std it seems to be faster:
```
go-callvis -nostd .
```

Output svg:
```
go-callvis -file example_callvis -format svg .
```
(The file extension will be added automatically to `example_callvis`)

`-nostd` sometimes does not produce a graph...
