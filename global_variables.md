Global variables are often considered as bad code.

Disadvantages are that your functions are not independend of the rest of the code.

## alternatives to global variables

#### function that simply returns values

Instead of having a global string slice that contains key value pairs \
you could have a function that just returns this string slice.\
Then you have those key value pairs encapsulated and also you make sure \
that these values are immutable.\
This is useful since Go does not support constant slices.

=> But then your function is still not independent of the rest of your code.\
It depends on that return function

Another question is:\
Will you pass that return function as an argument to your function or \
will you just use it inside your function?

When you use it as a function argument your function is more independend \
and you can just pass different values in your unit test.\
If these values are meant to be constants though you will never want to pass anything else \
but that function, so it does not make sense.
