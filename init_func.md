An `init()` function will always be executed first.\
It will be executed before `main()`.

You can have multiple `init()` functions in your program.\
You can even have multiple `init()` functions in one file.

#### example

If you have these two files:

`foo.go`:
```
package main

import "fmt"

func init() {
	fmt.Println("FOOOOOOOOOO")
}
```

`main.go`:
```
package main

import "fmt"

func main() {
	fmt.Println("vim-go")
}
```

and a Module:\
`go.mod`
```
module cli_flags_hell

go 1.23.1
```

Output:
```
$ go run .
FOOOOOOOOOO
vim-go
```
