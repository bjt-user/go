https://go.dev/tour/flowcontrol/1

There are no while loops only for loops.

However you can just use a for loop like a while loop:
```
package main

func main() {
	sum := 1
	for sum < 10 {
		println(sum)
		sum += 1
	}
}
```

#### c-style for loops

```
for i := 0; i < 10; i++ {
    sum += i
}
```

#### for statements with range clause (iterate through slices)

```
package main

func main() {
	names := []string{"Sam", "George", "Mike"}

	for index, value := range names {
		println(index, value)
	}
}
```
This will output:
```
0 Sam
1 George
2 Mike
```

And in this case you have to use index, otherwise the compiler will throw an error.

If you dont need the index you can do it like this:
```
package main

func main() {
	names := []string{"Sam", "George", "Mike"}

	for _, value := range names {
		println(value)
	}
}
```

Or you just use the index in the slice:
```
package main

func main() {
	names := []string{"Sam", "George", "Mike"}

	for index := range names {
		println(names[index])
	}
}
```
