#### multiple return values

Methods may return multiple values, and returning a result, err pair is the \
conventional way a method indicates an error to its caller in Go.

#### public functions

Functions starting with uppercase letters are "public" and can be called from other packages.

Also called "exported".

https://go.dev/tour/basics/3

#### example function

```
func is_heading(line string) bool {
	if strings.HasPrefix(line, "#") {
		return true
	} else {
		return false
	}
}
```

#### example: call by reference to change struct inside function

```
package main

import "fmt"

type cli_args struct {
	file_path string
	force     bool
}

func change_cli_args(args *cli_args) {
	args.file_path = "foo.txt"
	args.force = true
}

func main() {
	my_args := cli_args{file_path: "bar.txt", force: false}
	fmt.Println(my_args)

	change_cli_args(&my_args)
	fmt.Println(my_args)
}
```

#### variable number of arguments (variadic parameters)

With `...` in front of a datatype you can specify that you can pass \
any number of values and it will be stored in a slice.
```
package main

import "fmt"

func string_list(arg ...string) {
        fmt.Printf("%v (%T)\n", arg, arg)
}

func main() {
        string_list("foo", "bar", "another string")

        string_list("one", "two")
}
```
outputs:
```
[foo bar another string] ([]string)
[one two] ([]string)
```

#### passing a slice as a variadic parameter

You have to put the `...` operator after the slice name:
```
func string_list(arg ...string) {
        fmt.Printf("%v (%T)\n", arg, arg)
}

func main() {
        myslice := []string{"foo", "bar", "test"}

        string_list(myslice...)
}
```
