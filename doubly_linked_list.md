https://en.wikipedia.org/wiki/Doubly_linked_list

> The first and last nodes of a doubly linked list for all practical applications are immediately accessible (i.e., accessible without traversal, and usually called head and tail) and therefore allow traversal of the list from the beginning or end of the list, respectively: e.g., traversing the list from beginning to end, or from end to beginning, in a search of the list for a node with specific data value.\
Any node of a doubly linked list, once obtained, can be used to begin a new traversal of the list, in either direction (towards beginning or end), from the given node.

#### using head and tail

https://stackoverflow.com/questions/29862122/practicality-of-head-and-tail-in-a-linked-list

> At the start of the linked list,both head and tail point to null.\
When a new node is added, both of them point to the new element.\
But, again, just as soon as a new element is added again, the head points at the first element always, whereas, the tail points to the new element added.

Using a tail should make appending much simpler.

It should make accessing nodes that are closer to the tail than to the head faster.
