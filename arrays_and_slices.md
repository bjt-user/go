https://go.dev/blog/slices-intro

In `go` arrays have a fixed length and cannot be resized.\
"Slices" on the other hand are like arrays in other languages, and they are dynamic and \
can be resized.

> Arrays have their place, but they’re a bit inflexible, so you don’t see them too often in Go code.
Slices, though, are everywhere. They build on arrays to provide great power and convenience.

## arrays

This is an array:
```
package main

import "fmt"

func main() {
	my_names := [3]string{"Peter", "Mark", "Tim"}

	fmt.Println(my_names)
	fmt.Println("There are", len(my_names), "elements in the array.")
}
```
**Warning: You can't seem to use the "println" from the builtin package on arrays!**

## slices

A slice literal is declared just like an array literal, except you leave out the element count:
```
letters := []string{"a", "b", "c", "d"}
```

#### initialize slice

https://pkg.go.dev/builtin@go1.23.1#make

There are at least two ways to initialize an empty slice.

```
var names []string
```

or use `make`:
```
names := make([]string, 0)
```

Both ways produce the same output.\
Not sure what the internal difference is.

#### last element

```
letters := []string{"a", "b", "c", "d"}

fmt.Println(letters[len(letters)-1])
```

#### append an element to a slice

```
package main

import "fmt"

func main() {
	my_names := []string{"Peter", "Mark", "Tim", "Tom"}

	my_names = append(my_names, "Bruno")

	fmt.Println(my_names)
	fmt.Println("There are", len(my_names), "elements in the array.")
}
```

## pitfalls

#### ranged for loops: value does not update

```
package main

import "fmt"

func main() {
	var my_names []string = []string{"Peter", "Timo", "Timmy"}

	for index, value := range my_slice {
		if index == 1 {
			my_names[index] = "Frankenstein"
		}
		fmt.Printf("%d: %v\n", index, value)
	}
}
```

Even though we set the second name to Frankenstein, the program will output:
```
0: Peter
1: Timo
2: Timmy
```

So use `my_names[index]` to print the slice inside the loop like this:
```
package main

import "fmt"

func main() {
	var my_names []string = []string{"Peter", "Timo", "Timmy"}

	for index, _ := range my_names {
		if index == 1 {
			my_names[index] = "Frankenstein"
		}
		fmt.Printf("%d: %v\n", index, my_names[index])
	}
}
```
and it will output the updated slice:
```
0: Peter
1: Frankenstein
2: Timmy
```
