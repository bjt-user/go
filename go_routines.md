https://go.dev/tour/concurrency/1

#### first example

```
package main

import (
	"fmt"
	"time"
)

func sleep_five_sec() {
	fmt.Println("Start sleeping 5 sec.")
	time.Sleep(5 * time.Second)
	fmt.Println("Finished sleeping 5 sec.")
}

func sleep_ten_sec() {
	fmt.Println("Start sleeping 10 sec.")
	time.Sleep(10 * time.Second)
	fmt.Println("Finished sleeping 10 sec.")
}

func main() {
	go sleep_ten_sec()
	sleep_five_sec()
}
```

will produce this:
```
$ go run main.go 
Start sleeping 5 sec.
Start sleeping 10 sec.
Finished sleeping 5 sec.
```

So the go routine will start after the next function and not even finish.
