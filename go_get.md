#### TODO: no package to get

When trying to get in the golang/tools module:
```
.../golang/tools (master) $ GOMODCACHE="$(pwd)/.godeps" go get
go: no package to get in current directory
```

It only downloads lists and version numbers:
```
$ tree
.
└── cache
    └── download
        ├── github.com
        │   ├── google
        │   │   └── go-cmp
        │   │       └── @v
        │   │           ├── list
        │   │           └── v0.6.0.mod
        │   └── yuin
        │       └── goldmark
        │           └── @v
        │               ├── list
        │               └── v1.4.13.mod
...
```

Even when I remove everything from the default GOMODCACHE, it shows the same thing.
```
~/go/pkg/mod $ ls -la
total 8
drwxr-xr-x 2 bf wheel 4096 Mar  6 07:21 .
drwxr-xr-x 4 bf wheel 4096 Feb 22 06:45 ..
```
